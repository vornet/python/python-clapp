import typing


class UsageError(ValueError):
    pass


class AppError(RuntimeError):
    pass


class AppBug(RuntimeError):
    pass


class HelpRequested(RuntimeError):
    pass


class UnknownArgumentUsageError(UsageError):
    pass


class MissingValueUsageError(UsageError):
    pass


class BadValueUsageError(UsageError):

    def __init__(self,
                 option_desc: str,
                 type_desc: str,
                 value: typing.Any):
        self.option_desc = option_desc
        self.type_desc = type_desc
        self.value = value

    def __str__(self):
        return str(tuple([
            self.option_desc,
            self.type_desc,
            self.value,
        ]))


class MissingArgumentUsageError(UsageError):
    pass


class ParamsError(ValueError):
    pass
