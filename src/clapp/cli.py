import abc
import re
import typing

import clapp.err
from clapp.dbg import DEV


"""
!!! DRAFT !!!


        It's often advisable to provide own factories, especially if they
        can help validate the value without significant side effect.  For
        example, let's imagine we accept '--mountpoint' parameter, which
        must be an absolute path.  We can move the validation of this
        condition earlier by passing its own factory:

            class MountPoint:

                @classmethod
                def from_arg(cls, path):
                    if not path.startswith('/'):
                        raise ValueError(
                            f"mountpoint path must be absolute: {path}"
                        )
                    return cls(path)

                def __init__(self, path):
                    ...     # here the path is alrerdy checked

            options={'MountPoint':
                     KeyValue('--mount', factory=MountPoint.from_arg}

        Note that one should only present a failed usage error when it's
        guarranteed that the problem is in the usage of the CLI.  For example,
        while passing a non-existent file could be an usage error (typo) but
        it can just as easily be that the file is supposed to exist but does
        not; raising such condition as usage error would be misleading.

"""

class FactoryIF(abc.ABC):

    @abc.abstractmethod
    def cook(self, raw_value: typing.Any) -> typing.Any:
        pass


class PullResultIF(abc.ABC):

    @property
    @abc.abstractmethod
    def spec(self) -> typing.Any:
        # .. well, here ^^ we really want Pattern or something, but it would
        # by cyclic reference at this point.
        pass

    @property
    @abc.abstractmethod
    def rest(self) -> list[str]:
        pass

    @property
    @abc.abstractmethod
    def value(self) -> typing.Any:
        pass

    @property
    @abc.abstractmethod
    def is_match(self) -> bool:
        pass


class ArgConditionIF(abc.ABC):
    """
    Condition to match argument string against specification
    """

    @abc.abstractmethod
    def __init__(self, spec):
        """
        Just keep condition specification *spec* for later when check is made
        """
        pass

    @abc.abstractmethod
    def check(self, value: str) -> bool:
        """
        True if *value* matches this rule
        """
        pass


class ParamIF(abc.ABC):
    """
    Basic parameter specification.
    """

    @property
    @abc.abstractmethod
    def default(self) -> typing.Any:
        """
        Default value if the option or positional argument is never matched.
        """
        pass

    @property
    @abc.abstractmethod
    def factory(self) -> typing.Union[FactoryIF, None]:
        """
        Factory to run any relevant values through.

        Typically, factory will be a callable that accepts single argument
        and returns value that should appear as the parameter value.

        The exact value passed to the factory will depend on parameter
        implementation.  For example, in case of KeyValue, the value following
        the key will be passed.  In case of ScalarPos, the matched positional
        argument, etc.

        A very common example:

            options={'Size': KeyValue('-s|--size', factory=int)}

        will, with arguments `['--size', '30'] result in parameter:

            {'Size': 30}

        Notice that the parameter is already converted to int.

        If a factory raises ValueError, it will be re-raised as
        clapp.err.BadValueUsageError.

        Passing a non-callable as *factory* during initialization will result
        in a factory that always returns the same value.  This can be useful
        for literal arguments that should be trigger a different value than
        itself.

        For example, if '--cleanup' argument is passed to this pattern:

            Pattern(positionals=[
                ('Action', LiteralPos('--cleanup', factory='cleanup')),
            ])

        the resulting parameter will be:

            {'Action': 'cleanup'}

        """
        pass

    @property
    @abc.abstractmethod
    def cond(self) -> typing.Optional[ArgConditionIF]:
        """
        Specification of matching condition.

        Exact matching method depends on parameter type and this value.

        For options, the condition is typically string containing all accepted
        forms of the option name joined with comma (',') or pipe sign ('|').
        For example,

            {'BAR': KeyValue('-b|--bar')}

        Passing list with individual option names is equivalent to the joined
        string, so  the above example is equivalent to:

            {'BAR': KeyValue(['-b', '--bar'])}

        If callable (eg. function) is passed, it must accept a single string
        argument and will be called with each command line argument.  If the
        resulting value is true, then the argument has matched.  The above
        example could be written as:

            {'BAR': KeyValue(lambda a: a in ['-b', '--bar'])}

        For positional parameters, the condition is optional, but can be used
        to achieve detection of optional positional parameters that are
        followed by more mandatory parameters.  For example:

            PAT = Pattern(positionals=[
                ('FOO', ScalarPos()),
                ('BAR', ScalarPos(lambda a: '/' in a)),
                ('BAZ', ScalarPos()),
            ])

        can match:

            PAT.parse(['alice', 'bob'])
            -> {
                'FOO': 'alice',
                'BAZ': 'bob',
            }

            PAT.parse(['alice', 'some/path', 'bob'])
            -> {
                'FOO': 'alice',
                'BAR': 'some/path',
                'BAZ': 'bob',
            }

        There are several pre-defined callables that detect common string
        forms:

         *  C_ANY - any string (default for positional parameters),
         *  C_GNU_LONG - GNU long-style argument (eg. '--foo-bar'),
         *  C_GNU_SHORT - GNU short-style argument (eg. '-b'),
         *  C_GNU_TERMINATOR - GNU terminator, precisely '--',
         *  C_NONESUCH -- unmatchable condition (for internal use),
         *  C_NONOPT -- string not starting with dash,
         *  C_UNIX_ABSPATH -- string starting with forward slash (eg. '/foo'),
         *  C_UNIX_PATH -- string containing forward slash (eg. 'foo/bar'),
         *  C_XORG_LONG -- XOrg long-style argument (eg' -foo'),


        so the previous example could be also defined as:

            PAT = Pattern(positionals=[
                ('FOO', ScalarPos()),
                ('BAR', ScalarPos(C_UNIX_PATH)),
                ('BAZ', ScalarPos()),
            ])

        """
        pass

    @property
    @abc.abstractmethod
    def trigger(self) -> dict[str, typing.Any]:
        """
        Extra dictionary that should be merged when this parameter has matched>

        Each parameter can specify extra dictionary that will be merged
        into final parameter set when the given parameter has been matched,
        regardless of the parameter type or matching circumstances.

        For example, if a pattern like this:

            FB_PAT = Pattern(positionals=[
                ('FOO', ScalarPos()),
                ('BAR', MaybeScalarPos(trigger={'BAR_SEEN': 'yes'})),
            ])

        is called with single argument:

            FB_PAT.parse(['Alice'])
            -> {'FOO': 'Alice'}

        but with two arguments:

            FB_PAT.parse(['Alice', 'Bob'])
            -> {'FOO': 'Alice', 'BAR': 'Bob', 'BAR_SEEN': 'yes'}
        """
        pass

    @abc.abstractmethod
    def pull(self, args: list[str]) -> PullResultIF:
        """
        Parse maximum possible arguments from *args*

        Match all known patterns in *args* and return pull result containing
        any matched values, reference to matched parameters and remaining
        arguments.
        """
        pass


class ArgFactory(FactoryIF):

    @classmethod
    def from_arg(cls, arg, desc=None):
        if arg is None:
            return EchoFactory()
        if type(arg) is dict:
            return MapFactory(vmap=arg, desc=desc)
        if callable(arg):
            return FuncFactory(fn=arg, desc=desc)
        else:
            return LiteralFactory(value=arg, desc=desc)

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}({self.desc})"

    def __repr__(self):
        return str(self)


class FuncFactory(FactoryIF):
    """
    Factory that will run assigned function on the raw value
    """

    def __init__(self, fn, desc=None):
        self.fn = fn
        self.desc = desc or fn.__name__

    def cook(self, raw_value: typing.Any) -> typing.Any:
        return self.fn(raw_value)


class LiteralFactory(FactoryIF):
    """
    Factory that will return assigned value, no matter the raw value
    """

    def __init__(self, value, desc=None):
        self.value = value
        self.desc = desc or repr(value)

    def cook(self, raw_value):
        return self.value


class EchoFactory(FactoryIF):
    """
    Factory that will simply return the raw value
    """

    def cook(self, raw_value):
        return raw_value

    def __str__(self):
        return 'EchoFactory()'

    def __repr__(self):
        return str(self)


class MapFactory(FactoryIF):
    """
    Factory that will run different literal based on dictionary
    """

    def desc(self):
        parts = [f'{k}->{v}' for k, v in self.vmap.items]
        return ','.join(parts)

    def __init__(self, vmap, desc=None):
        self.vmap = vmap
        self.desc = desc or vmap.__name__

    def cook(self, raw_value: typing.Any) -> typing.Any:
        return self.vmap.get(raw_value)


class Param(ParamIF):
    pass


class Option(Param):

    default = None
    factory = None
    cond = None
    trigger: dict[str, typing.Any] = {}

    def __init__(self, cond, factory=None, default=None, trigger=None):
        self.cond = ArgCondition.from_spec(cond)
        self.factory = ArgFactory.from_arg(factory)
        self.default = default
        self.trigger = trigger or {}
        self._param_name = None

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}({self.cond})"


class Pos(Param):

    default = None
    factory = None
    cond = None
    trigger: dict[str, typing.Any] = {}

    def __init__(self, cond=None, factory=None, default=None, trigger=None):
        self.factory = ArgFactory.from_arg(factory)
        self.cond = ArgCondition.from_spec(cond)
        self.default = default
        self.trigger = trigger or {}
        self._param_name = None

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}({self._param_name})"

    def __repr__(self):
        return str(self)


class PullResult(PullResultIF):

    is_match = True

    spec = None
    rest: list[str] = []
    value = None

    def __init__(self, spec, rest, value=None):
        self.spec = spec
        self.rest = rest
        self.value = value

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}(v={self.value},s={self.spec},r={self.rest})"

    def __bool__(self):
        return self.is_match


class DefaultPullResult(PullResult):
    pass


class NoPullResult(PullResult):

    is_match = False


class KeyValue(Option):

    def pull(self, args):
        # DEV.VAL(self, 'args', args)
        if not args:
            return NoPullResult(self, args)
        if not self.cond.check(args[0]):
            return NoPullResult(self, args)
        if len(args) < 2:
            raise clapp.err.MissingValueUsageError(str(self))
        try:
            DEV.VAL(self, 'self.factory', self.factory)
            cooked = self.factory.cook(args[1])
        except ValueError:
            raise clapp.err.BadValueUsageError(
                option_desc=f"{self._param_name} in {self}",
                type_desc=self.factory.desc,
                value=args[1],
            ) from None
        return PullResult(self, args[2:], value=cooked)


class Enum(Option):

    #
    #     {'MODE': Enum({
    #         '-f|--mode-foo': 'foo_m',
    #         '-b|--mode-bar': 'bar_m',
    #     })}
    #

    def __init__(self, cond, trigger=None):
        if type(cond) is not dict:
            raise ValueError(
                f"invalid Enum condition specification: not a dict: {cond}"
            )
        super().__init__(cond=cond, trigger=trigger)

    def pull(self, args):
        def fallback():
            if self.cond.has_default():
                value = self.cond.get_default()
                return DefaultPullResult(self, args, value)
            return NoPullResult(self, args)
        if not args:
            return fallback()
        if not self.cond.check(args[0]):
            return fallback()
        return PullResult(self, args[1:], self.cond.enum.get(args[0]))

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}({self.cond})"


class Bool(Option):

    def __init__(self, cond, inverse=None, default=None, trigger=None):
        super().__init__(cond=cond, default=default, trigger=trigger)
        #
        # If user has not provided 'inverse' condition, fall back to
        # inverse condition that will never match.  (This is opposite
        # to what is normally done with conditions; if they are not
        # provided, anything matches.)
        #  \/
        self.inverse = ArgCondition.from_spec(inverse, fallback=C_NONESUCH)
        """
        Condition that matches this flag's inverse variant.

        For example, if an option 'Foo' is a boolean which can be turned
        on by '-f' and turned off by '-F', it should appear in the Pattern
        as:

            options={'Foo': Bool(cond='-f', inverse='-F')}

        If *inverse* is omitted, it cannot match, i.e. the option can only
        be turned on but not turned off.

        Another common example could be "quiet" versus "verbose" mode (here
        with long options as well):

            options={'Verbose': Bool(cond='-v|--verbose',
                                     inverse='-q|--quiet')}
        """

    def pull(self, args):
        if not args:
            return NoPullResult(self, args)
        if self.cond.check(args[0]):
            return PullResult(self, args[1:], value=True)
        if self.inverse.check(args[0]):
            return PullResult(self, args[1:], value=False)
        return NoPullResult(self, args)


class MaybeScalarPos(Pos):

    def __init__(self, cond=None, factory=None, default=None, trigger=None):
        super().__init__(cond=cond, factory=factory, default=default, trigger=trigger)

    def pull(self, args):
        if not args:
            return DefaultPullResult(self, args, value=self.default)
        if not self.cond.check(args[0]):
            return NoPullResult(self, args)
        try:
            cooked = self.factory.cook(args[0])
        except ValueError:
            return DefaultPullResult(self, args, value=self.default)
        return PullResult(self, args[1:], value=cooked)


class ScalarPos(Pos):

    def pull(self, args):
        if not args:
            return NoPullResult(self, args)
        if not self.cond.check(args[0]):
            return NoPullResult(self, args)
        try:
            cooked = self.factory.cook(args[0])
        except ValueError:
            raise clapp.err.BadValueUsageError(
                option_desc=f"{self._param_name} in {self}",
                type_desc=self.factory.desc,
                value=args[0],
            ) from None
        return PullResult(self, args[1:], value=cooked)


class LiteralPos(Pos):   # AKA "command"

    def __init__(self, cond, factory=None, trigger=None):
        super().__init__(cond=cond, trigger=trigger, factory=factory)

    def pull(self, args):
        if not args:
            return NoPullResult(self, args)
        if not self.cond.check(args[0]):
            return NoPullResult(self, args)
        return PullResult(self, args[1:], self.factory.cook(args[0]))

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}({self.cond})"


class EnumPos(Pos):   # AKA "command"

    class Default:
        pass

    def __init__(self, cond, trigger=None):
        super().__init__(trigger=trigger)
        self.cond = EnumCondition(cond)

    def pull(self, args):
        def fallback():
            if self.cond.has_default():
                value = self.cond.get_default()
                return DefaultPullResult(self, args, value)
            return NoPullResult(self, args)
        if not args:
            return fallback()
        if not self.cond.check(args[0]):
            return fallback()
        return PullResult(self, args[1:], self.cond.enum.get(args[0]))

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}({self.cond})"


class NonemptyListPos(Pos):

    def pull(self, args):
        out = []
        togo = args.copy()
        while togo:
            if not self.cond.check(togo[0]):
                break
            arg = togo.pop(0)
            try:
                out.append(self.factory.cook(arg))
            except ValueError:
                raise clapp.err.BadValueUsageError(
                    option_desc=f"{self._param_name} in {self}",
                    type_desc=self.factory.desc,
                    value=arg,
                ) from None
        if not out:
            return NoPullResult(self, args)
        return PullResult(self, togo, value=out)


class ListPos(Pos):

    def pull(self, args):
        out = []
        togo = args.copy()
        while togo:
            if not self.cond.check(togo[0]):
                break
            arg = togo.pop(0)
            try:
                out.append(self.factory.cook(arg))
            except ValueError:
                raise clapp.err.BadValueUsageError(
                    option_desc=f"{self._param_name} in {self}",
                    type_desc=self.factory.desc,
                    value=arg,
                ) from None
        return PullResult(self, togo, value=out)


class NoOption(Option):

    def __init__(self):
        pass

    def pull(self, args):
        return NoPullResult(self, args)

    def __str__(self):
        return 'N'


class ArgCondition(ArgConditionIF):

    @classmethod
    def from_spec(cls, spec, fallback=None):
        if spec is None:
            if fallback is None:
                return FunctionalCondition(C_ANY)
            return FunctionalCondition(fallback)
        if callable(spec):
            return FunctionalCondition(spec)
        if type(spec) is dict:
            return EnumCondition(spec)
        if type(spec) is list:
            return LiteralCondition(spec)
        if type(spec) is str:
            return LiteralCondition(spec)
        raise ValueError(f"invalid condition specification: {spec}")

    def __init__(self, cond):
        self.cond = cond
        self.desc = self.__class__.__name__ + '()'

    def __str__(self):
        return self.desc

    def __repr__(self):
        return self.desc


def C_ANY(arg: str) -> bool:
    return True


def C_NONESUCH(arg: str) -> bool:
    return False


def C_NONOPT(arg: str) -> bool:
    return not arg.startswith('-')


def C_GNU_SHORT(arg: str) -> bool:
    return len(arg) == 2 and arg.startswith('-')


def C_GNU_LONG(arg: str) -> bool:
    return len(arg) > 2 and arg.startswith('--')


def C_GNU_TERMINATOR(arg: str) -> bool:
    return arg == '--'


def C_XORG_LONG(arg: str) -> bool:
    return len(arg) > 2 and arg.startswith('-') and not arg.startswith('--')


def C_UNIX_PATH(arg: str) -> bool:
    return '/' in arg


def C_UNIX_ABSPATH(arg: str) -> bool:
    return arg.startswith('/')


def F_POSINT(arg: str) -> int:
    if not arg:
        raise ValueError(f"not an integer: {arg!r}")
    try:
        n = int(arg)
    except ValueError:
        raise ValueError(f"not an integer: {arg!r}")
    if n < 0:
        raise ValueError(f"not a positive integer: {n!r}")
    return n


def F_ID(arg: str) -> str:
    if not re.match(r'^[a-zA-Z_][a-zA-Z_0-9]*$', arg):
        raise ValueError(f"not a valid ID: {arg!r}")
    return arg


def F_KEY(arg: str) -> str:
    if not re.match(r'^[a-zA-Z_0-9./:-]+$', arg):
        raise ValueError(f"not a valid KEY: {arg!r}")
    return arg


def F_NONEMPTY(arg: str) -> str:
    if not arg:
        raise ValueError(f"not a valid value: {arg!r} is empty")
    return arg


def F_ANY(arg: str) -> typing.Any:
    return arg


def F_KVMAP(arg: str) -> dict[str, typing.Union[str, bool]]:
    """
    Simple KEY1=VALUE1,KEY2,KEY3=VALUE2.. map

    Similar to mapping such as fstab mount options, this format is
    constrained to an ID-like string as KEY and simple string
    (prohibiting commas, semicolons, whitespace and more) as VALUE.

    Arbitrary amount of KEY=VALUE pairs can be provided, and if two
    pairs have the same KEY, the first one is ignored.

    VALUE can be omitted, in which case the value is set to True.
    """
    out: dict = {}
    key: str
    value: typing.Union[str, bool]
    for part in arg.split(','):
        if not part:
            continue
        if '=' in part:
            key, value = part.split('=', maxsplit=1)
        else:
            key, value = part, True
        out[F_KEY(key)] = value
    return out


class S_DEFAULT:
    """
    Symbol to indicate default item in a key/value map
    """
    pass


_BUILTIN_CONDITIONS: set[typing.Callable] = {
    C_ANY,
    C_GNU_LONG,
    C_GNU_SHORT,
    C_GNU_TERMINATOR,
    C_NONESUCH,
    C_NONOPT,
    C_UNIX_ABSPATH,
    C_UNIX_PATH,
    C_XORG_LONG,
}


_BUILTIN_FACTORIES: set[typing.Callable] = {
    F_ANY,
    F_ID,
    F_KEY,
    F_KVMAP,
    F_NONEMPTY,
    F_POSINT,
}


_BUILTIN_SYMBOLS: set[type] = {
    S_DEFAULT,
}


class FunctionalCondition(ArgCondition):

    def __init__(self, spec):
        self.spec = spec
        self.desc = spec.__name__

    def check(self, value: str):
        return self.spec(value)


class EnumCondition(ArgCondition):

    def _normalize(self, spec):
        def breakup(key, value):
            if type(key) is not str:
                return {key: value}
            out = {}
            for subkey in key.replace(',', '|').split('|'):
                out[subkey] = value
            return out
        normal = {}
        for key, value in spec.items():
            normal.update(breakup(key, value))
        return normal

    def _describe(self, spec):
        items = '|'.join([
            k for k in spec.keys()
            if k is not S_DEFAULT
        ])
        if S_DEFAULT in spec:
            return f"[{items}]"
        return f"{items}"

    def has_default(self):
        return S_DEFAULT in self.enum

    def get_default(self):
        if S_DEFAULT in self.enum:
            return self.enum[S_DEFAULT]
        return None

    def __init__(self, spec):
        self.enum = self._normalize(spec)
        self.desc = self._describe(spec)

    def check(self, value: str):
        return value in self.enum


class LiteralCondition(ArgCondition):

    def _normalize(self, spec):
        return spec.replace(',', '|').split('|')

    def __init__(self, spec):
        self.known_values = self._normalize(spec)
        self.desc = '|'.join(self.known_values)

    def check(self, value: str):
        return value in self.known_values


class SetIF(abc.ABC):

    @property
    @abc.abstractmethod
    def item_factory(self) -> type:
        pass

    @abc.abstractmethod
    def __init__(self, specs):
        pass

    @abc.abstractmethod
    def pull(self, args: list[str]) -> tuple[dict, list[str]]:
        pass


class Set(SetIF):

    def __str__(self):
        cn = self.__class__.__name__
        s = '|'.join([str(i) for i in self.items])
        return f"{cn}({s})"

    def __init__(self, items=None):
        self.items = items or self.item_factory()

    def __bool__(self):
        return bool(self.items)


class ParamSet(Set):

    @abc.abstractmethod
    def _update_names(self) -> None:
        """
        Let param specs know names of the associated params

        Specs need this for error and debug message creation.
        """
        pass

    def __init__(self, items=None):
        super().__init__(items=items)
        self._update_names()


class PatternSet(Set):

    item_factory = list

    def add(self, spec):
        self.items.append(spec)

    def pull(self, args):
        def try1(P):
            try:
                params = P.parse_all(togo)
            except clapp.err.UsageError:
                return {}, togo
            return params, []
        if not self.items:
            DEV.MSG(self, '(no patterns)', pfx=' >')
            return {}, args
        togo = args.copy()
        for pattern in self.items:
            DEV.MSG(self, f'Trying pattern: {pattern} on {togo}', pfx=' >')
            parsed_params, extras = try1(pattern)
            if not parsed_params:
                DEV.MSG(self, f'no match! ({args})', pfx=' !')
                continue
            DEV.MSG(self, f'match! parsed_params={parsed_params}', pfx=' !')
            return pattern.trigger | parsed_params, []
        raise clapp.err.UsageError(f"no usage pattern matched: {args}")


class OptionSet(ParamSet):

    item_factory = dict

    def _update_names(self):
        for k, v in self.items.items():
            v._param_name = k

    def add(self, name, spec):
        if name in self.items:
            raise ValueError(
                f"option already defined: {name}:{self.items[name]}"
            )
        self.items[name] = spec
        self._update_names()

    def pull(self, args):
        if not self.items:
            DEV.MSG(self, '(no options)', pfx=' >')
            return {}, args
        togo = args.copy()
        params = {}
        diff = 1
        while diff and togo:
            base = len(togo)
            for oname, ospec in self.items.items():
                DEV.MSG(self, f'Trying: {oname}:{ospec} on {togo}', pfx=' >')
                pulled = ospec.pull(togo)
                if pulled:
                    DEV.MSG(self, f'pulled={pulled}', pfx=' .')
                    DEV.MSG(self, f'pulled.rest={pulled.rest}', pfx=' .')
                    DEV.MSG(self, f'pulled.value={pulled.value}', pfx=' .')
                    togo = pulled.rest
                    params.update({oname: pulled.value})
                    params.update(pulled.spec.trigger)
            diff = base - len(togo)
            DEV.MSG(self, f'diff={diff}')
        DEV.MSG(self, f'params={params}', pfx=' _')
        return params, togo


class PositionalSet(ParamSet):

    item_factory = list

    def _update_names(self):
        for k, v in self.items:
            v._param_name = k

    def add(self, name, spec):
        self.items[name] = spec
        self._update_names()

    def pull(self, args):
        if not self.items:
            DEV.MSG(self, '(no positionals)', pfx=' >')
            return {}, args
        togo = args.copy()
        params = {}
        for pname, pspec in self.items:
            DEV.MSG(self, f'Trying: {pname}/{pspec} on {togo}', pfx=' >')
            pulled = pspec.pull(togo)
            togo = pulled.rest
            if pulled:
                DEV.MSG(self, f'pulled={pulled}', pfx=' .')
                DEV.MSG(self, f'pulled.rest={pulled.rest}', pfx=' .')
                DEV.MSG(self, f'pulled.value={pulled.value}', pfx=' .')
                params.update({pname: pulled.value})
                params.update(pulled.spec.trigger)
            else:
                if togo:
                    raise clapp.err.UnknownArgumentUsageError(repr(togo[0]))
                raise clapp.err.MissingArgumentUsageError(str(pspec))
        DEV.MSG(self, f'params={params}', pfx=' _')
        DEV.MSG(self, f'togo={togo}', pfx=' _')
        return params, togo


class Pattern:

    def __init__(self, options=None, positionals=None,
                 patterns=None,
                 trigger=None):
        self.option_set = OptionSet(options)
        self.positional_set = PositionalSet(positionals)
        self.pattern_set = PatternSet(patterns)
        self.trigger = trigger or {}

    def __str__(self):
        cn = self.__class__.__name__
        parts = []
        if self.option_set:
            parts.append(f"o={self.option_set}")
        if self.positional_set:
            parts.append(f"p={self.positional_set}")
        if self.pattern_set:
            parts.append(f"t={self.pattern_set}")
        desc = ','.join(parts)
        return f"{cn}({desc})"

    def __repr__(self):
        return str(self)

    def add(self, name, spec):
        if isinstance(spec, Option):
            self.add_option(name, spec)
        elif isinstance(spec, Pos):
            self.add_positional(name, spec)
        else:
            raise ValueError(
                f"spec must be positional or option: {spec}"
            )

    def add_positional(self, name, spec):
        self.positional_set.add((name, spec))

    def add_pattern(self, spec):
        self.pattern_set.add(spec)

    def add_option(self, name, spec):
        self.option_set.add((name, spec))

    def pull(self, args):
        """
        Wrapper to allow recursive parsing of *args* as part of PositionalSet.

        Try to parse and remove a number of items, wrap result into
        PullResultIF subclass and return it.
        """
        try:
            params, togo = self.parse(args)
        except clapp.err.UsageError:
            return NoPullResult(self, args)
        return PullResult(self, togo, params)

    def parse(self, args):
        """
        Parse *args*, return parameter dictionary and remaining arguments.

        Use all means (options, positionals and patterns) to interpret all
        arguments and store result in parameter dictionary.  Return tuple
        of that dictionary and any remaning arguments.
        """
        DEV.reset_depth()
        togo = args.copy()
        from_options, togo = self.option_set.pull(togo)
        from_positionals, togo = self.positional_set.pull(togo)
        from_patterns, togo = self.pattern_set.pull(togo)
        params = from_options | from_positionals | from_patterns | self.trigger
        return params, togo

    def parse_all(self, args):
        """
        Fully parse *args*.

        Parse *args* argument list using *parse()* and return parameter
        dictionary if, and only if, the parsing was successful and exhaustive.

        Raise clapp.err.UnknownArgumentUsageError in case parsing left any
        arguments unprocessed.
        """
        params, togo = self.parse(args)
        if togo:
            raise clapp.err.UnknownArgumentUsageError(repr(togo[0]))
        return params


P_HELP = Pattern(
    positionals=[('_', LiteralPos('--help'))],
    trigger={'clapp.action': clapp.app._show_help_a},
)

_SP_HELP = Pattern(
    positionals=[('_', LiteralPos('--help'))],
    trigger={'clapp.action': 'clapp.app._show_help_a'},
)

_SP_VERSION = Pattern(
    positionals=[('_', LiteralPos('--version'))],
    trigger={'clapp.action': 'clapp.app._show_version_a'},
)

P_VERSION = Pattern(
    positionals=[('_', LiteralPos('--version'))],
    trigger={'clapp.action': clapp.app._show_version_a},
)
