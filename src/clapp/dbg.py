from dataclasses import dataclass
import os
import sys
import inspect


DEFAULT_DEVOPTS = {
    'deprecations': True,
    'debug': False,
}


@dataclass
class DevOpts:
    deprecations: bool = True
    debug: bool = False

    @classmethod
    def from_env(cls):
        args = DEFAULT_DEVOPTS.copy()
        parts = os.environ.get('CLAPP_DEV_OPTS', '').split(',')
        for part in parts:
            if part.startswith('no') and part.removeprefix('no') in args:
                args[part.removeprefix('no')] = False
            elif part in args:
                args[part] = True
            else:
                continue
        if (os.environ.get('CLAPP_DEBUG') == 'true' or os.environ.get('DEBUG') == 'true'):
            args['debug'] = True
        return cls(**args)


def eprint(text):
    print(text, file=sys.stderr)


@dataclass
class Devlogger:
    dev_opts: DevOpts
    base_depth: int

    @classmethod
    def new(cls, base_depth=0):
        return cls(
            base_depth=base_depth,
            dev_opts=DevOpts.from_env(),
        )

    def reset_depth(self):
        if not self.dev_opts.debug:
            return
        if self.base_depth:
            return
        depth = len(inspect.stack())
        self.base_depth = depth

    def MSG(self, obj, msg, pfx='  '):
        if not self.dev_opts.debug:
            return
        try:
            cref = obj.__class__.__name__ + '.'
        except AttributeError:
            cref = ''
        caller_fn = inspect.stack()[1].function
        depth = len(inspect.stack())
        depth_pfx = pfx * (depth - self.base_depth)
        prefix = f'{depth_pfx} {cref}{caller_fn}():'
        eprint(f"DEBUG:{prefix}{msg}")

    def DEPRECATION(self, app, msg: str, hints: list[str] = []):
        if not app.dev_opts.deprecations:
            return
        eprint(f"DEPRECATION for {app.name} v{app.version}: {msg}")
        hiding_hints = [
            "To hide this message, set .dev_opts.deprecations to False or set",
            "'nodeprecations' in CLAPP_DEV_OPTS environment variable",
        ]
        for hint in hints + hiding_hints:
            eprint(f" .. hint: {hint}")

    def OBJ(self, po, name, obj, pfx='  ', ev='->'):
        if not self.dev_opts.debug:
            return
        self.MSG(po, f' {ev} {name}={obj}', pfx=pfx)
        if not hasattr(obj, 'OX'):
            return
        pd = ' ' * len(ev)
        for line in obj.OX:
            self.MSG(po, f' {pd} {name}{line}', pfx=pfx)

    def VAL(self, obj, name, val, pfx='  '):
        if not self.dev_opts.debug:
            return
        try:
            cref = obj.__class__.__name__ + '.'
        except AttributeError:
            cref = ''
        caller_fn = inspect.stack()[1].function
        depth = len(inspect.stack())
        depth_pfx = pfx * (depth - self.base_depth)
        prefix = f'{depth_pfx} {cref}{caller_fn}():'
        eprint(f"DEBUG:{prefix}{name}={val!r}")

    def M_M(self, obj, r, pfx='  '):
        if not self.dev_opts.debug:
            return
        self.MSG(obj, f'match! result={r}', pfx=' .')
#       self.MSG(obj, f'       result.rest={r.rest}', pfx=' .')
#       self.MSG(obj, f'       result.params={r.params}', pfx=' .')


DEV = Devlogger.new()
