from __future__ import annotations


from dataclasses import dataclass
from datetime import datetime, timedelta
from enum import Enum
from typing import Iterable
import subprocess
import neaty.log


def _noop(*args, **kwargs) -> None:
    pass


def _join_lines(lines: Iterable[str]) -> str:
    return ''.join([ln + '\n' for ln in lines])


class LogPolicy(Enum):
    ALWAYS = 'always'
    ONFAIL = 'onfail'
    NEVER = 'never'


@dataclass(frozen=True, slots=True)
class Result:
    action: Action
    out: str
    err: str
    es: int
    timing: TimePeriod

    def is_failure(self) -> bool:
        return self.es > 0

    def is_success(self) -> bool:
        return self.es == 0

    def _fmt_payload(self) -> str:
        action_name = self.action.name
        verb = (
            f"exited with: {self.es}"
            if self.is_failure()
            else "succeeded"
        )
        if self.action.action_type == ActionType.COMMAND:
            action_args = ' '.join(self.action.args)
            return f"{action_name}: command finished: {verb} (unquoted here); {action_args}"
        if self.action.action_type == ActionType.SCRIPT:
            return f"{action_name}: script finished: {verb}"
        raise ValueError(f"invalid action type: {self.action.action_type}")

    def _log_failing(self) -> None:
        neaty.log.warn(self._fmt_payload())
        for line in self.out.splitlines():
            neaty.log.warn("  out:|" + line)
        for line in self.err.splitlines():
            neaty.log.warn("  err:|" + line)

    def _log_successful(self) -> None:
        neaty.log.think(self._fmt_payload())
        for line in self.err.splitlines():
            neaty.log.warn(line)

    def log(self, policy: LogPolicy = LogPolicy.ONFAIL) -> None:
        fn = self._log_successful if self.es == 0 else self._log_failing
        fn()


class ActionType(Enum):
    COMMAND = 'command'
    SCRIPT = 'script'


@dataclass(frozen=True, slots=True)
class Action:
    args: list[str]
    name: str
    dry: bool
    action_type: ActionType
    code: str | None = None

    @classmethod
    def from_code(cls,
                  code: str,
                  name: str,
                  dry: bool = False,
                  ):
        return cls(
            args=['sh', '-c', code],
            name=name,
            dry=dry,
            action_type=ActionType.SCRIPT,
            code=code,
        )

    @classmethod
    def from_cmd(cls,
                 cmd: list[str],
                 name: str,
                 dry: bool = False,
                 ):
        return cls(
            args=cmd,
            name=name,
            dry=dry,
            action_type=ActionType.COMMAND,
        )

    def _run_with_stdin(self, stdin: str) -> tuple[int, str, str]:
        p = subprocess.Popen(
            self.args,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        out, err = p.communicate(input=stdin.encode())
        return p.returncode, out.decode(), err.decode()

    def _run_without_stdin(self) -> tuple[int, str, str]:
        p = subprocess.Popen(
            self.args,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        out, err = p.communicate()
        return p.returncode, out.decode(), err.decode()

    def run(self, stdin: str | None = None) -> Result:
        start_dt = datetime.now()
        if self.dry:
            return Result(
                action=self,
                out='',
                err='',
                es=0,
                timing=TimePeriod.since(start_dt),
            )
        es, out, err = (
            self._run_without_stdin()
            if stdin is None
            else self._run_with_stdin(stdin)
        )
        return Result(
            action=self,
            out=out,
            err=err,
            es=es,
            timing=TimePeriod.since(start_dt),
        )


@dataclass(frozen=True, slots=False)
class TimePeriod:
    start: datetime
    end: datetime
    start_ts: str
    end_ts: str
    duration_td: timedelta

    @classmethod
    def new(cls,
            start: datetime | str,
            end: datetime | str,
            ) -> TimePeriod:
        """
        Create a TimePeriod from *start* and *end*.

        Both *start* and *end* can be either datetime.datetime or an
        ISO-8601 formatted string.
        """
        start_dt, start_ts = _dt_and_ts(start)
        end_dt, end_ts = _dt_and_ts(end)
        return cls(
            start=start_dt,
            end=end_dt,
            start_ts=start_ts,
            end_ts=end_ts,
            duration_td=end_dt - start_dt,
        )

    @classmethod
    def since(cls, start: datetime) -> TimePeriod:
        """
        Create a TimePeriod from *start* to now.

        *start* must be a datetime.datetime object.
        """
        end = datetime.now()
        return cls(
            start=start,
            end=end,
            start_ts=start.isoformat(),
            end_ts=end.isoformat(),
            duration_td=end - start,
        )


def _describe_cmd(cmd: list[str],
                  name: str,
                  ) -> Iterable[str]:
    togo = cmd.copy()
    yield "-- begin cmd %s --" % name
    if len(togo) == 0:
        yield '# no command passed to run_cmd()'
    elif len(togo) == 1:
        yield quote(togo[0])
    else:
        yield togo.pop(0) + '\\'
        while len(togo) > 1:
            yield '  ' + togo.pop(0) + '\\'
        yield '  ' + togo.pop(0)
    yield "-- end cmd %s --" % name


def _describe_code(code: str,
                   name: str,
                   ) -> Iterable[str]:
    yield "-- begin code %s --" % name
    for line in code.split('\n'):
        yield '|' + line.rstrip('\n')
    yield "-- end code %s --" % name


def _dt_and_ts(dt_or_ts: datetime | str) -> tuple[datetime, str]:
    """
    Disambiguate date item *dt_or_ts( to identical datetime and ISO string

    *dt_or_ts* must be datetime.datetime object or an ISO-8601 formatted
    string.  Whichever one is provided, compute the other one and return
    both as tuple:  datetime.datetime and str.
    """
    if isinstance(dt_or_ts, datetime):
        return dt_or_ts, dt_or_ts.isoformat()
    return datetime.fromisoformat(dt_or_ts), dt_or_ts


def stdout_of(cmd: list[str],
              stdin: str | None = None,
              name: str = 'stdout_of()',
              log_policy: LogPolicy = LogPolicy.ONFAIL,
              ) -> str:
    result = Action.from_cmd(cmd, name).run(stdin)
    result.log(policy=log_policy)
    if not result.is_success():
        raise RuntimeError("command failed")
    return result.out


def stdout_of_code(code: str,
                   stdin: str | None = None,
                   name: str = 'stdout_of_code()',
                   log_policy: LogPolicy = LogPolicy.ONFAIL,
                   ) -> str:
    result = Action.from_code(code, name).run(stdin)
    result.log(policy=log_policy)
    if not result.is_success():
        raise RuntimeError("command failed")
    return result.out


def run_cmd(cmd: list[str],
            stdin: str | None = None,
            name: str = 'run_cmd()',
            dry: bool = False,
            log_policy: LogPolicy = LogPolicy.ALWAYS,
            ) -> bool:
    if dry:
        for line in _describe_cmd(cmd, name):
            neaty.log.warn(line)
        return True
    result = Action.from_cmd(cmd, name or 'run_cmd()').run(stdin)
    result.log(policy=log_policy)
    return result.is_success()


@dataclass(frozen=True, slots=False)
class SyntaxErrorMessage:
    msg: str
    preview: list[str]
    line_no: int | None = None
    char_no: int | None = None


@dataclass(frozen=True, slots=False)
class _BashSyntaxErrorLine:
    content: str
    is_preview: bool
    line_no: int | None = None


def bash_syntax_errors(code: str) -> list[SyntaxErrorMessage]:

    def parse_errline(errline: str) -> _BashSyntaxErrorLine:
        if not errline.startswith('bash: line '):
            # dumb case
            return _BashSyntaxErrorLine(
                content=errline,
                is_preview=False,
            )
        parts = (
            errline
            .removeprefix('bash: line ')
            .split(':', maxsplit=1)
        )
        lineno = int(parts[0])
        rest = parts[1].lstrip()
        is_preview, content = (
            (True, rest[1:-1])
            if (rest.startswith('`') and rest.endswith("'"))
            else (False, rest)
        )
        return _BashSyntaxErrorLine(
            content=content,
            is_preview=is_preview,
            line_no=lineno,
        )

    def collect_messages(err_lines) -> Iterable[SyntaxErrorMessage]:
        #
        # they come in pairs like this:
        #
        #     bash: line <NUM>: <ERROR>
        #     bash: line <NUM>: `<LINE_PREVIEW>'
        #
        # where <NUM> should be the same, <ERROR> is Bash's description of
        # the error and quotes around <LINE_PREVIEW> are a backtick and a
        # single quote.
        #
        # Example:
        #
        #     bash: line 1: syntax error near unexpected token `;;'
        #     bash: line 1: `echo foo; ;; ; echo bar'
        #
        togo = err_lines.copy()
        preview_buf = []
        last_msg = None
        while togo:
            this = parse_errline(errline=togo.pop(0))
            if this.is_preview:
                preview_buf.append(this.content)
                continue
            if last_msg is None:
                last_msg = this
                continue
            yield SyntaxErrorMessage(
                msg=last_msg.content,
                preview=preview_buf,
            )
            preview_buf = []
        if last_msg is None:
            return
        yield SyntaxErrorMessage(
            msg=last_msg.content,
            preview=preview_buf,
        )

    action = Action.from_cmd(
        cmd=['bash', '-n'],
        name='clapp.sh.bash_syntax_errors()',
    )
    result = action.run(stdin=code)
    msgs = list(collect_messages(result.err.splitlines()))
    if result.is_success() and msgs:
        raise RuntimeError(f"panic: `bash -n` exited with zero but reported errors: {msgs}")
    return msgs


def bash_maybe_syntax_error(code: str,
                            location: str | None = None,
                            ) -> str | None:
    def collect_parts(e: SyntaxErrorMessage) -> Iterable[str]:
        yield "syntax error found:"
        if location is not None:
            yield f"    location={location}"
        yield f"    msg={e.msg!r}"
        yield f"    line=`{e.preview[0]}`"
    syntax_errors = bash_syntax_errors(code=code)
    if not syntax_errors:
        return None
    return '\n'.join(collect_parts(syntax_errors[0]))


def bash_syntax_ok(code: str,
                   log_policy: LogPolicy = LogPolicy.ONFAIL,
                   ) -> bool:
    action = Action.from_cmd(
        cmd=['bash', '-n'],
        name='clapp.sh.bash_syntax_ok()',
    )
    result = action.run(stdin=code)
    result.log(policy=log_policy)
    return result.is_success()


def run_code(code: str,
             stdin: str | None = None,
             name: str = 'run_code()',
             dry: bool = False,
             log_policy: LogPolicy = LogPolicy.ALWAYS,
             ) -> bool:
    if dry:
        for line in _describe_code(code, name):
            neaty.log.warn(line)
        return True
    result = Action.from_code(code, name).run()
    result.log(policy=log_policy)
    return result.is_success()


def dry_run_code(code: str,
                 stdin: str | None = None,
                 name: str = 'dry_run_code()',
                 ):
    for line in _describe_code(code, name):
        neaty.log.warn(line)


def quote_words(words: Iterable[str]) -> list[str]:
    for word in words:
        if '\n' in word:
            raise ValueError(f"newlines are not allowed: word={word!r}")
    cmd = ['bash', '-c', "printf '%q\\n' \"$@\"", '--']
    cmd.extend(words)
    return stdout_of(cmd).splitlines()


def quote(text: str) -> str:
    return stdout_of([
        'bash', '-c', 'printf %q "$1"', '--', text
    ])


def content_equals(path_a: str,
                   path_b: str,
                   ) -> bool:
    with open(path_a) as f:
        ca = f.read()
    with open(path_b) as f:
        cb = f.read()
    return ca == cb
