"""
A simple application
"""

import abc

import neaty.log as LOG


class SpecIF(abc.ABC):

    @property
    @abc.abstractmethod
    def source_type(self):
        pass

    @property
    @abc.abstractmethod
    def output_type(self):
        pass

    def resolve(self, source):
        pass


class Spec(SpecIF):

    @classmethod
    def from_arg(self, arg):
        if arg.startswith('/'):
            return RegexSpec(arg[1:])
        if arg.startswith('='):
            return RegexSpec(arg[1:])

    def __init__(self, arg):
        self.arg = arg


class MethodIF:

    pass


class ValueSpec(Spec):

    pass


class RegexSpec(Spec):

    pass


class ExpressionSpec(Spec):

    pass


class JsonStdinSpec(Spec):

    pass


class PlainStdinSpec(Spec):

    pass
