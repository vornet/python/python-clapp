import clapp.cling
import clapp.err
from .if_ import ArgConditionIF


class ArgCondition(ArgConditionIF):

    @classmethod
    def from_spec(cls, spec, fallback=None):
        if spec is None:
            if fallback is None:
                return FunctionalCondition(clapp.cling.C_ANY)
            return FunctionalCondition(fallback)
        if callable(spec):
            return FunctionalCondition(spec)
        if type(spec) is dict:
            return EnumCondition(spec)
        if type(spec) is list:
            return LiteralCondition(spec)
        if type(spec) is str:
            return LiteralCondition(spec)
        raise ValueError(f"invalid condition specification: {spec}")

    def __init__(self, cond):
        self.cond = cond
        self.desc = self.__class__.__name__ + '()'

    def __str__(self):
        return self.desc

    def __repr__(self):
        return self.desc


class FunctionalCondition(ArgCondition):

    def __init__(self, spec):
        self.spec = spec
        self.desc = spec.__name__

    def check(self, value: str):
        return self.spec(value)


class EnumCondition(ArgCondition):

    def _normalize(self, spec):
        def breakup(key, value):
            if type(key) is not str:
                return {key: value}
            out = {}
            for subkey in key.replace(',', '|').split('|'):
                out[subkey] = value
            return out
        normal = {}
        for key, value in spec.items():
            normal.update(breakup(key, value))
        return normal

    def _describe(self, spec):
        items = '|'.join([
            k for k in spec.keys()
            if k is not clapp.cling.S_DEFAULT
        ])
        if clapp.cling.S_DEFAULT in spec:
            return f"[{items}]"
        return f"{items}"

    def has_default(self):
        return clapp.cling.S_DEFAULT in self.enum

    def get_default(self):
        if clapp.cling.S_DEFAULT in self.enum:
            return self.enum[clapp.cling.S_DEFAULT]
        return None

    def __init__(self, spec):
        self.enum = self._normalize(spec)
        self.desc = self._describe(spec)

    def check(self, value: str):
        return value in self.enum


class LiteralCondition(ArgCondition):

    def _normalize(self, spec):
        return spec.replace(',', '|').split('|')

    def __init__(self, spec):
        self.known_values = self._normalize(spec)
        self.desc = '|'.join(self.known_values)

    def check(self, value: str):
        return value in self.known_values
