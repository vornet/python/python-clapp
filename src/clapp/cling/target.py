import hashlib

from .if_ import TargetIF


class Target(TargetIF):

    name = None
    is_collective = False

    @classmethod
    def from_str(cls, name=None):
        if name is None:
            return FlatTarget('FLAT')
        if name.startswith('+'):
            return CollectiveTarget(name[1:])
        return cls(name)

    def __init__(self, name=None):
        self.name = name or ''

    def __gt__(self, othr):
        return str(self) > str(othr)

    def __str__(self):
        cn = self.__class__.__name__
        hid = hashlib.md5(str(id(self)).encode()).hexdigest()[:4]
        return f"{cn}({self.name}.{hid})"

    def __repr__(self):
        return str(self)


class CollectiveTarget(Target):

    is_collective = True


class FlatTarget(Target):

    pass
