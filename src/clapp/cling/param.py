import typing

import clapp.err
from .if_ import ParamIF
from .argcondition import ArgCondition, EnumCondition
from .argfactory import ArgFactory
from .target import FlatTarget, Target
from .pullresult import PullResult, ResultLog, NoPullResult, DefaultPullResult
from clapp.dbg import DEV


class Param(ParamIF):

    @property
    def children(self):
        return self._children

    @property
    def default(self):
        return self._default

    @property
    def tgt(self):
        if not self._tgt:
            self._tgt = FlatTarget('ADHOC')
        return self._tgt

    def _emit_result(self, rest, value=None):
        return PullResult(
            spec=self,
            rest=rest,
            values=[value],
        )

    def _emit_nonresult(self, rest):
        return NoPullResult(
            spec=self,
            rest=rest,
        )

    def _emit_default(self, rest):
        return DefaultPullResult(
            spec=self,
            rest=rest,
            values=[self.default],
        )

    def init(self, tgt):
        self._tgt = tgt


class Option(Param):

    factory = None
    cond = None
    trigger: dict[str, typing.Any] = {}

    def __init__(self, cond, factory=None, default=None, trigger=None):
        self.cond = ArgCondition.from_spec(cond)
        self.factory = ArgFactory.from_arg(factory)
        self._default = default
        self.trigger = trigger or {}
        self._tgt = None

    def __str__(self):
        cn = self.__class__.__name__
        return f"[{self._tgt}]{cn}({self.cond})"


class Pos(Param):

    factory = None
    cond = None
    trigger: dict[str, typing.Any] = {}

    def __init__(self, cond=None, factory=None, default=None, trigger=None):
        self.factory = ArgFactory.from_arg(factory)
        self.cond = ArgCondition.from_spec(cond)
        self._default = default
        self.trigger = trigger or {}
        self._tgt = None

    def __str__(self):
        cn = self.__class__.__name__
        return f"[self._tgt]{cn}()"

    def __repr__(self):
        return str(self)


class KeyValue(Option):

    def pull(self, args):
        # DEV.VAL(self, 'args', args)
        if not args:
            return self._emit_nonresult(args)
        if not self.cond.check(args[0]):
            return self._emit_nonresult(args)
        if len(args) < 2:
            raise clapp.err.MissingValueUsageError(str(self))
        try:
            DEV.V(self, 'self.factory', self.factory)
            cooked = self.factory.cook(args[1])
        except ValueError:
            raise clapp.err.BadValueUsageError(
                option_desc=f"{self._tgt} in {self}",
                type_desc=self.factory.desc,
                value=args[1],
            ) from None
        return self._emit_result(args[2:], value=cooked)


class Enum(Option):

    #
    #     {'MODE': Enum({
    #         '-f|--mode-foo': 'foo_m',
    #         '-b|--mode-bar': 'bar_m',
    #     })}
    #

    def __init__(self, cond, trigger=None):
        if type(cond) is not dict:
            raise ValueError(
                    f"invalid Enum condition specification: not a dict: {cond}"
            )
        super().__init__(cond=cond, trigger=trigger)

    @property
    def default(self):
        return self.cond.get_default()

    def pull(self, args):
        def fallback():
            if self.cond.has_default():
                return self._emit_default(args)
            return self._emit_nonresult(args)
        if not args:
            return fallback()
        if not self.cond.check(args[0]):
            return fallback()
        return self._emit_result(args[1:], self.cond.enum.get(args[0]))

    def __str__(self):
        cn = self.__class__.__name__
        return f"[{self._tgt}]{cn}({self.cond})"


class Bool(Option):

    def __init__(self, cond, inverse=None, default=None, trigger=None):
        super().__init__(cond=cond, default=default, trigger=trigger)
        #
        # If user has not provided 'inverse' condition, fall back to
        # inverse condition that will never match.  (This is opposite
        # to what is normally done with conditions; if they are not
        # provided, anything matches.)
        #  \/
        self.inverse = ArgCondition.from_spec(inverse, fallback=clapp.cli.C_NONESUCH)
        """
        Condition that matches this flag's inverse variant.

        For example, if an option 'Foo' is a boolean which can be turned
        on by '-f' and turned off by '-F', it should appear in the Pattern
        as:

            options={'Foo': Bool(cond='-f', inverse='-F')}

        If *inverse* is omitted, it cannot match, i.e. the option can only
        be turned on but not turned off.

        Another common example could be "quiet" versus "verbose" mode (here
        with long options as well):

            options={'Verbose': Bool(cond='-v|--verbose',
                                     inverse='-q|--quiet')}
        """

    def pull(self, args):
        if not args:
            return self._emit_nonresult(args)
        if self.cond.check(args[0]):
            return self._emit_result(args[1:], value=True)
        if self.inverse.check(args[0]):
            return self._emit_result(args[1:], value=False)
        return self._emit_nonresult(args)


class MaybeScalarPos(Pos):

    def __init__(self, factory=None, default=None, trigger=None):
        super().__init__(factory=factory, default=default, trigger=trigger)

    def pull(self, args):
        if not args:
            return self._emit_default(args)
        try:
            cooked = self.factory.cook(args[0])
        except ValueError:
            return self._emit_default(args)
        return self._emit_result(args[1:], value=cooked)


class ScalarPos(Pos):

    def pull(self, args):
        if not args:
            return self._emit_nonresult(args)
        if not self.cond.check(args[0]):
            return self._emit_nonresult(args)
        try:
            cooked = self.factory.cook(args[0])
        except ValueError:
            raise clapp.err.BadValueUsageError(
                option_desc=f"{self._tgt} in {self}",
                type_desc=self.factory.desc,
                value=args[0],
            ) from None
        return self._emit_result(args[1:], value=cooked)


class LiteralPos(Pos):   # AKA "command"

    def __init__(self, cond, factory=None, trigger=None):
        super().__init__(cond=cond, trigger=trigger, factory=factory)

    def pull(self, args):
        if not args:
            return self._emit_nonresult(args)
        if not self.cond.check(args[0]):
            return self._emit_nonresult(args)
        return self._emit_result(args[1:], self.factory.cook(args[0]))

    def __str__(self):
        cn = self.__class__.__name__
        return f"[{self._tgt}]{cn}({self.cond})"


class EnumPos(Pos):   # AKA "command"

    class Default:
        pass

    def __init__(self, cond, trigger=None):
        super().__init__(trigger=trigger)
        self.cond = EnumCondition(cond)

    @property
    def default(self):
        return self.cond.get_default()

    def pull(self, args):
        def fallback():
            if self.cond.has_default():
                return self._emit_default(args)
            return self._emit_nonresult(args)
        if not args:
            return fallback()
        if not self.cond.check(args[0]):
            return fallback()
        return self._emit_result(args[1:], self.cond.enum.get(args[0]))

    def __str__(self):
        cn = self.__class__.__name__
        return f"[{self._tgt}]{cn}({self.cond})"


class NonemptyListPos(Pos):

    def pull(self, args):
        out = []
        for arg in args:
            if not self.cond.check(arg):
                break
            try:
                out.append(self.factory.cook(arg))
            except ValueError:
                raise clapp.err.BadValueUsageError(
                    option_desc=f"{self._tgt} in {self}",
                    type_desc=self.factory.desc,
                    value=arg,
                ) from None
        if not out:
            return self._emit_nonresult(args)
        return self._emit_result([], value=out)


class ListPos(Pos):

    def pull(self, args):
        out = []
        for arg in args:
            if not self.cond.check(arg):
                break
            try:
                out.append(self.factory.cook(arg))
            except ValueError:
                raise clapp.err.BadValueUsageError(
                    option_desc=f"{self._tgt} in {self}",
                    type_desc=self.factory.desc,
                    value=arg,
                ) from None
        return self._emit_result([], value=out)


class NoOption(Option):

    def __init__(self):
        pass

    def pull(self, args):
        return self._emit_nonresult(args)

    def __str__(self):
        return 'N'


class ParamSet(Param):

    factory = None
    cond = None
    trigger: dict[str, typing.Any] = {}

    def __bool__(self):
        return bool(self.items)

    def __init__(self, pspecs=None):
        self.items = []
        self._tgt = None
        for pname, pspec in pspecs or []:
            self.add(pspec, pname)

    def __str__(self):
        cn = self.__class__.__name__
        s = '|'.join([str(i) for i in self.items])
        return f"[{self._tgt}]{cn}({s})"

    def add(self, spec, name=None):
        spec.init(Target.from_str(name))
        self.items.append(spec)

    def by_name(self, name):
        for i in self.items:
            if i.name == name:
                return i
        raise KeyError("no such spec: {name!r}")


class OrderedParamSet(ParamSet):

    def pull(self, args):
        if not self.items:
            DEV.MSG(self, '(no positionals)', pfx=' >')
            return self._emit_nonresult(args)
        if not args:
            DEV.MSG(self, '(no args)', pfx=' >')
            return self._emit_nonresult(args)
        togo = args.copy()
        result_log = ResultLog(self, togo)
        for pspec in self.items:
            DEV.MSG(self, f'OrderedParamSet.Trying: {pspec} on {togo}', pfx=' >')
            result = pspec.pull(togo)
            togo = result.rest
            if result:
                DEV.O(self, 'result', result, ev='match!')
                result_log.add(result)
            else:
                if togo:
                    raise clapp.err.UnknownArgumentUsageError(repr(togo[0]))
                raise clapp.err.MissingArgumentUsageError(str(pspec))
        DEV.MSG(self, f'result_log={result_log}', pfx=' _')
        DEV.MSG(self, f'togo={togo}', pfx=' _')
        return result_log


class AlternativeParamSet(ParamSet):

    def pull(self, args):
        def try1(spec, args):
            try:
                return spec.pull(args.copy())
            except (clapp.err.MissingArgumentUsageError,
                    clapp.err.UnknownArgumentUsageError):
                return self._emit_nonresult()
        if not self.items:
            DEV.MSG(self, '(no patterns)', pfx=' >')
            return self._emit_nonresult(args)
        for pspec in self.items:
            DEV.MSG(self, f'Trying pattern: {pspec} on {args}', pfx=' >')
            result = try1(pspec, args)
            if not result:
                DEV.MSG(self, f'no match! ({args})', pfx=' !')
                continue
            if result.rest:
                DEV.MSG(self, f'partial match: {args} - {result.rest}', pfx=' !')
                continue
            DEV.O(self, 'result', result, ev='match!')
            return result
        raise clapp.err.UsageError(f"no usage pattern matched: {args}")


class UnorderedParamSet(ParamSet):

    def pull(self, args):
        if not self.items:
            DEV.MSG(self, '(no options)', pfx=' >')
            return self._emit_nonresult(args)
        togo = args.copy()
        result_log = ResultLog(self, togo)
        diff = 1
        while diff and togo:
            base = len(togo)
            DEV.MSG(self, f' ..... result_log={result_log}', pfx='++')
            for ospec in self.items:
                DEV.MSG(self, f'Trying: {ospec} on {togo}', pfx=' >')
                result = ospec.pull(togo)
                if result:
                    DEV.O(self, 'result', result, ev='match!')
                    togo = result.rest
                    result_log.add(result)
            diff = base - len(togo)
            DEV.MSG(self, f'diff={diff}')
        DEV.MSG(self, f'result_log={result_log}', pfx=' _')
        if not result_log:
            return self._emit_nonresult(args)
        return result_log


class OptionSet(UnorderedParamSet):
    pass


class PatternSet(AlternativeParamSet):
    pass


class PositionalSet(OrderedParamSet):
    pass


class Pattern(Param):

    default = None
    cond = None
    factory = None
    trigger: dict[str, typing.Any] = {}

    def __init__(self, options=None, positionals=None,
                 patterns=None,
                 trigger=None):
        self._option_set = OptionSet(options.items() if options else [])
        self._option_set.init(FlatTarget())
        self._positional_set = PositionalSet(positionals)
        self._positional_set.init(FlatTarget())
        self._pattern_set = PatternSet(patterns)
        self._pattern_set.init(FlatTarget())
        self.trigger = trigger or {}

    def __str__(self):
        cn = self.__class__.__name__
        parts = []
        if self._option_set:
            parts.append(f"o={self._option_set}")
        if self._positional_set:
            parts.append(f"p={self._positional_set}")
        if self._pattern_set:
            parts.append(f"t={self._pattern_set}")
        desc = ','.join(parts)
        return f"[^]{cn}({desc})"

    def __repr__(self):
        return str(self)

    def add(self, spec, name=None):
        if isinstance(spec, Option):
            self.add_option(name, spec)
        elif isinstance(spec, Pos):
            self.add_positional(name, spec)
        else:
            raise ValueError(
                f"spec must be positional or option: {spec}"
            )

    def add_positional(self, spec, name=None):
        self._positional_set.add(spec, name)

    def add_pattern(self, spec, name='_'):
        self._pattern_set.add(spec, name)

    def add_option(self, spec, name=None):
        self._option_set.add(spec, name)

    def pull(self, args):
        """
        Wrapper to allow recursive parsing of *args* as part of PositionalSet.

        Try to parse and remove a number of items, wrap result into
        PullResultIF subclass and return it.
        """
        try:
            params, togo = self.parse(args)
        except clapp.err.UsageError:
            return self._emit_nonresult(args)
        return self._emit_result(togo, params)

    def parse(self, args):
        """
        Parse *args*, return parameter dictionary and remaining arguments.

        Use all means (options, positionals and patterns) to interpret all
        arguments and store result in parameter dictionary.  Return tuple
        of that dictionary and any remaning arguments.
        """
        DEV.reset_depth()
        togo = args.copy()
        from_options = self._option_set.pull(togo)
        DEV.V(self, 'from_options', from_options)
        DEV.V(self, 'from_options.rest', from_options.rest)
        DEV.V(self, 'from_options.params', from_options.params)
        from_positionals = self._positional_set.pull(from_options.rest)
        DEV.V(self, 'from_positionals', from_positionals)
        DEV.V(self, 'from_positionals.rest', from_positionals.rest)
        DEV.V(self, 'from_positionals.params', from_positionals.params)
        from_patterns = self._pattern_set.pull(from_positionals.rest)
        DEV.V(self, 'from_patterns', from_patterns)
        DEV.V(self, 'from_patterns.rest', from_patterns.rest)
        DEV.V(self, 'from_patterns.params', from_patterns.params)
        togo = from_patterns.rest
        params = (
            from_options.params
            | from_positionals.params
            | from_patterns.params
            | self.trigger
        )
        DEV.V(self, 'params', params)
        return params, togo

    def parse_all(self, args):
        """
        Fully parse *args*.

        Parse *args* argument list using *parse()* and return parameter
        dictionary if, and only if, the parsing was successful and exhaustive.

        Raise clapp.err.UnknownArgumentUsageError in case parsing left any
        arguments unprocessed.
        """
        params, togo = self.parse(args)
        if togo:
            raise clapp.err.UnknownArgumentUsageError(repr(togo[0]))
        return params


class CliPattern(OrderedParamSet):

    def __init__(self, options=None, positionals=None,
                 patterns=None,
                 trigger=None):
        super().__init__()
        self.add(OptionSet(options.items() if options else []))
        self.add(PositionalSet(positionals))
        self.add(PatternSet(patterns))
        self.trigger = trigger or {}
