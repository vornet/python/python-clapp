import abc
import typing


"""
!!! DRAFT !!!


        It's often advisable to provide own factories, especially if they
        can help validate the value without significant side effect.  For
        example, let's imagine we accept '--mountpoint' parameter, which
        must be an absolute path.  We can move the validation of this
        condition earlier by passing its own factory:

            class MountPoint:

                @classmethod
                def from_arg(cls, path):
                    if not path.startswith('/'):
                        raise ValueError(
                            f"mountpoint path must be absolute: {path}"
                        )
                    return cls(path)

                def __init__(self, path):
                    ...     # here the path is alrerdy checked

            options={'MountPoint':
                     KeyValue('--mount', factory=MountPoint.from_arg}

        Note that one should only present a failed usage error when it's
        guarranteed that the problem is in the usage of the CLI.  For example,
        while passing a non-existent file could be an usage error (typo) but
        it can just as easily be that the file is supposed to exist but does
        not; raising such condition as usage error would be misleading.

"""


class TargetIF(abc.ABC):

    @property
    @abc.abstractmethod
    def name(self) -> typing.Optional[str]:
        pass


class FactoryIF(abc.ABC):

    @abc.abstractmethod
    def cook(self, raw_value: typing.Any) -> typing.Any:
        pass


class PullResultIF(abc.ABC):

    @property
    @abc.abstractmethod
    def spec(self) -> typing.Any:
        # .. well, here ^^ we really want Pattern or something, but it would
        # by cyclic reference at this point.
        pass

    @property
    @abc.abstractmethod
    def params(self) -> dict[str, typing.Any]:
        pass

    @property
    @abc.abstractmethod
    def rest(self) -> list[str]:
        pass

    @property
    @abc.abstractmethod
    def is_match(self) -> bool:
        pass


class ResultLogIF(PullResultIF):

    @property
    @abc.abstractmethod
    def results(self) -> list[PullResultIF]:
        pass

    @abc.abstractmethod
    def add(self, result: PullResultIF):
        pass


class ArgConditionIF(abc.ABC):
    """
    Condition to match argument string against specification
    """

    @abc.abstractmethod
    def __init__(self, spec):
        """
        Just keep condition specification *spec* for later when check is made
        """
        pass

    @abc.abstractmethod
    def check(self, value: str) -> bool:
        """
        True if *value* matches this rule
        """
        pass


class ParamIF(abc.ABC):
    """
    Basic parameter specification.
    """

    @property
    @abc.abstractmethod
    def default(self) -> typing.Any:
        """
        Default value if the option or positional argument is never matched.
        """
        pass

    @property
    @abc.abstractmethod
    def factory(self) -> typing.Union[FactoryIF, None]:
        """
        Factory to run any relevant values through.

        Typically, factory will be a callable that accepts single argument
        and returns value that should appear as the parameter value.

        The exact value passed to the factory will depend on parameter
        implementation.  For example, in case of KeyValue, the value following
        the key will be passed.  In case of ScalarPos, the matched positional
        argument, etc.

        A very common example:

            options={'Size': KeyValue('-s|--size', factory=int)}

        will, with arguments `['--size', '30'] result in parameter:

            {'Size': 30}

        Notice that the parameter is already converted to int.

        If a factory raises ValueError, it will be re-raised as
        clapp.err.BadValueUsageError.

        Passing a non-callable as *factory* during initialization will result
        in a factory that always returns the same value.  This can be useful
        for literal arguments that should be trigger a different value than
        itself.

        For example, if '--cleanup' argument is passed to this pattern:

            Pattern(positionals=[
                ('Action', LiteralPos('--cleanup', factory='cleanup')),
            ])

        the resulting parameter will be:

            {'Action': 'cleanup'}

        """
        pass

    @property
    @abc.abstractmethod
    def cond(self) -> typing.Optional[ArgConditionIF]:
        """
        Specification of matching condition.

        Exact matching method depends on parameter type and this value.

        For options, the condition is typically string containing all accepted
        forms of the option name joined with comma (',') or pipe sign ('|').
        For example,

            {'BAR': KeyValue('-b|--bar')}

        Passing list with individual option names is equivalent to the joined
        string, so  the above example is equivalent to:

            {'BAR': KeyValue(['-b', '--bar'])}

        If callable (eg. function) is passed, it must accept a single string
        argument and will be called with each command line argument.  If the
        resulting value is true, then the argument has matched.  The above
        example could be written as:

            {'BAR': KeyValue(lambda a: a in ['-b', '--bar'])}

        For positional parameters, the condition is optional, but can be used
        to achieve detection of optional positional parameters that are
        followed by more mandatory parameters.  For example:

            PAT = Pattern(positionals=[
                ('FOO', ScalarPos()),
                ('BAR', ScalarPos(lambda a: '/' in a)),
                ('BAZ', ScalarPos()),
            ])

        can match:

            PAT.parse(['alice', 'bob'])
            -> {
                'FOO': 'alice',
                'BAZ': 'bob',
            }

            PAT.parse(['alice', 'some/path', 'bob'])
            -> {
                'FOO': 'alice',
                'BAR': 'some/path',
                'BAZ': 'bob',
            }

        There are several pre-defined callables that detect common string
        forms:

         *  C_ANY - any string (default for positional parameters),
         *  C_GNU_LONG - GNU long-style argument (eg. '--foo-bar'),
         *  C_GNU_SHORT - GNU short-style argument (eg. '-b'),
         *  C_GNU_TERMINATOR - GNU terminator, precisely '--',
         *  C_NONESUCH -- unmatchable condition (for internal use),
         *  C_NONOPT -- string not starting with dash,
         *  C_UNIX_ABSPATH -- string starting with forward slash (eg. '/foo'),
         *  C_UNIX_PATH -- string containing forward slash (eg. 'foo/bar'),
         *  C_XORG_LONG -- XOrg long-style argument (eg' -foo'),


        so the previous example could be also defined as:

            PAT = Pattern(positionals=[
                ('FOO', ScalarPos()),
                ('BAR', ScalarPos(C_UNIX_PATH)),
                ('BAZ', ScalarPos()),
            ])

        """
        pass

    @property
    @abc.abstractmethod
    def trigger(self) -> dict[str, typing.Any]:
        """
        Extra dictionary that should be merged when this parameter has matched>

        Each parameter can specify extra dictionary that will be merged
        into final parameter set when the given parameter has been matched,
        regardless of the parameter type or matching circumstances.

        For example, if a pattern like this:

            FB_PAT = Pattern(positionals=[
                ('FOO', ScalarPos()),
                ('BAR', MaybeScalarPos(trigger={'BAR_SEEN': 'yes'})),
            ])

        is called with single argument:

            FB_PAT.parse(['Alice'])
            -> {'FOO': 'Alice'}

        but with two arguments:

            FB_PAT.parse(['Alice', 'Bob'])
            -> {'FOO': 'Alice', 'BAR': 'Bob', 'BAR_SEEN': 'yes'}
        """
        pass

    @property
    @abc.abstractmethod
    def children(self):
        """
        Set of allowed sub-patterns.
        """
        pass

    @abc.abstractmethod
    def pull(self, args: list[str]) -> PullResultIF:
        """
        Parse maximum possible arguments from *args*

        Match all known patterns in *args* and return pull result containing
        any matched values, reference to matched parameters and remaining
        arguments.
        """
        pass

    @property
    @abc.abstractmethod
    def tgt(self) -> TargetIF:
        """
        Name (key) of the target parameter
        """
        pass

    @abc.abstractmethod
    def init(self, tgt: TargetIF):
        """
        Initialize with target.

        The parameter spec needs to know its target parameter name
        so that it can be passed to PullResult which can then use
        it to generate the parameter dictionary.

        We can't set it in __init__ though, due to how our syntax
        works.
        """
        pass

