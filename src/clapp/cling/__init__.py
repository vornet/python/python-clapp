import re
import typing

import clapp.err
from .if_ import ParamIF
from clapp.dbg import DEV


def parse(spec: ParamIF, args: list[str]) -> tuple[dict[str, typing.Any], list[str]]:
    """
    Parse *args* using *spec*, return parameter dict and remaining arguments.

    Use all means (options, positionals and patterns) to interpret all
    arguments and store result in parameter dictionary.  Return tuple
    of that dictionary and any remaning arguments.
    """
    DEV.reset_depth()
    togo = args.copy()
    results = spec.pull(togo)
    DEV.VAL(spec, 'results', results)
    return results.params, results.rest


def parse_all(spec: ParamIF, args):
    """
    Fully parse *args*.

    Parse *args* argument list using *parse()* and return parameter
    dictionary if, and only if, the parsing was successful and exhaustive.

    Raise clapp.err.UnknownArgumentUsageError in case parsing left any
    arguments unprocessed.
    """
    DEV.reset_depth()
    togo = args.copy()
    results = spec.pull(togo)
    DEV.VAL(spec, 'results', results)
    if results.rest:
        raise clapp.err.UnknownArgumentUsageError(repr(results.rest[0]))
    return results.params


def C_ANY(arg: str) -> bool:
    return True


def C_NONESUCH(arg: str) -> bool:
    return False


def C_NONOPT(arg: str) -> bool:
    return not arg.startswith('-')


def C_GNU_SHORT(arg: str) -> bool:
    return len(arg) == 2 and arg.startswith('-')


def C_GNU_LONG(arg: str) -> bool:
    return len(arg) > 2 and arg.startswith('--')


def C_GNU_TERMINATOR(arg: str) -> bool:
    return arg == '--'


def C_XORG_LONG(arg: str) -> bool:
    return len(arg) > 2 and arg.startswith('-') and not arg.startswith('--')


def C_UNIX_PATH(arg: str) -> bool:
    return '/' in arg


def C_UNIX_ABSPATH(arg: str) -> bool:
    return arg.startswith('/')


def F_POSINT(arg: str) -> int:
    if not arg:
        raise ValueError(f"not an integer: {arg!r}")
    try:
        n = int(arg)
    except ValueError:
        raise ValueError(f"not an integer: {arg!r}")
    if n < 0:
        raise ValueError(f"not a positive integer: {n!r}")
    return n


def F_ID(arg: str) -> str:
    if not re.match(r'^[a-zA-Z_][a-zA-Z_0-9]*$', arg):
        raise ValueError(f"not a valid ID: {arg!r}")
    return arg


def F_KEY(arg: str) -> str:
    if not re.match(r'^[a-zA-Z_0-9./:-]+$', arg):
        raise ValueError(f"not a valid KEY: {arg!r}")
    return arg


def F_NONEMPTY(arg: str) -> str:
    if not arg:
        raise ValueError(f"not a valid value: {arg!r} is empty")
    return arg


def F_ANY(arg: str) -> typing.Any:
    return arg


def F_KVMAP(arg: str) -> dict[str, typing.Union[str, bool]]:
    """
    Simple KEY1=VALUE1,KEY2,KEY3=VALUE2.. map

    Similar to mapping such as fstab mount options, this format is
    constrained to an ID-like string as KEY and simple string
    (prohibiting commas, semicolons, whitespace and more) as VALUE.

    Arbitrary amount of KEY=VALUE pairs can be provided, and if two
    pairs have the same KEY, the first one is ignored.

    VALUE can be omitted, in which case the value is set to True.
    """
    out: dict = {}
    key: str
    value: typing.Union[str, bool]
    for part in arg.split(','):
        if not part:
            continue
        if '=' in part:
            key, value = part.split('=', maxsplit=1)
        else:
            key, value = part, True
        out[F_KEY(key)] = value
    return out


class S_DEFAULT:
    """
    Symbol to indicate default item in a key/value map
    """
    pass


_BUILTIN_CONDITIONS: set[typing.Callable] = {
    C_ANY,
    C_GNU_LONG,
    C_GNU_SHORT,
    C_GNU_TERMINATOR,
    C_NONESUCH,
    C_NONOPT,
    C_UNIX_ABSPATH,
    C_UNIX_PATH,
    C_XORG_LONG,
}


_BUILTIN_FACTORIES: set[typing.Callable] = {
    F_ANY,
    F_ID,
    F_KEY,
    F_KVMAP,
    F_NONEMPTY,
    F_POSINT,
}


_BUILTIN_SYMBOLS: set[type] = {
    S_DEFAULT,
}
