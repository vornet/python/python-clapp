from .if_ import PullResultIF, ResultLogIF


class PullResult(PullResultIF):

    is_match = True

    spec = None
    rest: list[str] = []

    @property
    def OX(self):
        yield f'.rest={self.rest}'
        yield f'.params={self.params}'

    def __init__(self, spec, rest, values=None):
        self.spec = spec
        self.rest = rest
        self._values = values or []

    @property
    def params(self):
        return {self.spec.tgt: self._values} | self.spec.trigger

    def merge(self, othr):
        return PullResult(self, othr.rest, self._values+othr._values)

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}(v={self._values},s={self.spec},r={self.rest})"

    def __repr__(self):
        return str(self)

    def __bool__(self):
        return self.is_match


class ResultLog(ResultLogIF):

    spec = None

    def __init__(self, spec, args):
        self.spec = spec
        self._results = []
        self._args = args

    @property
    def is_match(self):
        return any([r.is_match for r in self._results])

    @property
    def params(self):

        def pmerge(r):
            for rpk, rpv in r.params.items():
                if rpk in merged and rpk.is_collective:
                    merged[rpk] = merged[rpk] + rpv
                else:   # result is new or overriding previous
                    merged[rpk] = rpv

        merged = {}
        for result in self._results:
            pmerge(result)
        if self.spec.trigger:
            merged.update(self.spec.trigger)
        return merged

    @property
    def rest(self):
        if not self._results:   # nothing matched yet..
            return self._args   # initial args
        return self._results[-1].rest

    @property
    def results(self):
        return self._results

    def add(self, result):
        self._results.append(result)

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}(p={self.params},s={self.spec},r={self.rest})"

    def __repr__(self):
        return str(self)

    def __bool__(self):
        return self.is_match


class DefaultPullResult(PullResult):
    pass


class NoPullResult(PullResult):

    is_match = False

    @property
    def params(self):
        return {}
