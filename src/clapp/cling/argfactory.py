from .if_ import FactoryIF


class ArgFactory(FactoryIF):

    @classmethod
    def from_arg(cls, arg, desc=None):
        if arg is None:
            return EchoFactory()
        if type(arg) is dict:
            return MapFactory(vmap=arg, desc=desc)
        if callable(arg):
            return FuncFactory(fn=arg, desc=desc)
        else:
            return LiteralFactory(value=arg, desc=desc)

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}({self.desc})"

    def __repr__(self):
        return str(self)


class FuncFactory(FactoryIF):
    """
    Factory that will run assigned function on the raw value
    """

    def __init__(self, fn, desc=None):
        self.fn = fn
        self.desc = desc or fn.__name__

    def cook(self, raw_value):
        return self.fn(raw_value)


class LiteralFactory(FactoryIF):
    """
    Factory that will return assigned value, no matter the raw value
    """

    def __init__(self, value, desc=None):
        self.value = value
        self.desc = desc or repr(value)

    def cook(self, raw_value):
        return self.value


class EchoFactory(FactoryIF):
    """
    Factory that will simply return the raw value
    """

    def cook(self, raw_value):
        return raw_value

    def __str__(self):
        return 'EchoFactory()'

    def __repr__(self):
        return str(self)


class MapFactory(FactoryIF):
    """
    Factory that will run different literal based on dictionary
    """

    def desc(self):
        parts = [f'{k}->{v}' for k, v in self.vmap.items]
        return ','.join(parts)

    def __init__(self, vmap, desc=None):
        self.vmap = vmap
        self.desc = desc or vmap.__name__

    def cook(self, raw_value):
        return self.vmap.get(raw_value)
