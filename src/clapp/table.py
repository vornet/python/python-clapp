from __future__ import annotations
from typing import Any


class TableError(IndexError):
    pass


class Table:

    @classmethod
    def from_data(cls,
                  rows: list[list[Any]],
                  wpadding: int = 1,
                  hpadding: int = 0,
                  ):
        trows = []
        for row in rows:
            trows.append(Row([
                Cell(cell) for cell
                in row
            ]))
        return cls(trows, wpadding, hpadding)

    def __init__(self,
                 rows: list[Row],
                 wpadding: int = 1,
                 hpadding: int = 0,
                 ):
        self.rows = rows
        for r in self.rows:
            r._table = self
        self.wpadding = wpadding
        self.hpadding = hpadding

    @property
    def width(self) -> int:
        return max([r.width for r in self.rows])

    @property
    def height(self) -> int:
        return len(self.rows)

    def column(self, n: int) -> Column:
        cells = []
        if n >= self.width:
            raise TableError('no such column: %s' % n)
        for row in self.rows:
            try:
                cell = row.cells[n]
            except IndexError:
                cell = Cell()
            cells.append(cell)
        return Column(cells)

    def columns(self) -> list[Column]:
        columns = []
        for n in range(0, self.width - 1):
            columns.append(self.column(n))
        return columns

    def row(self, n: int = 1) -> Row:
        try:
            row = self.rows[n]
        except IndexError:
            raise TableError("no such row: %s" % n)
        return row

    def fmt(self) -> list[str]:
        lines = []
        for row in self.rows:
            lines.extend(row.fmt())
        return lines


class Cell:

    def __init__(self, data: Any = None):
        self.data = data
        self._row: Row | None = None

    def __str__(self):
        return str(self.data)

    @property
    def dheight(self) -> int:
        return len(str(self).splitlines())

    @property
    def dwidth(self) -> int:
        return len(str(self))

    @property
    def height(self) -> int:
        return 1

    @property
    def width(self) -> int:
        return 1

    def fmt(self,
            w: int,
            h: int,
            ) -> list[str]:
        if w < self.dwidth:
            raise TableError('cannot fit into width: %d' % w)
        if h < self.dheight:
            raise TableError('cannot fit into height: %d' % h)
        lines = []
        togo = str(self).splitlines()
        while h:
            if togo:
                lines.append(('%%-%ds' % w) % togo.pop(0))
            else:
                lines.append('')
            h = h - 1
        return lines


class Row:

    def __init__(self, cells: list[Cell]):
        self.cells = cells
        self._table: Table | None = None
        for c in self.cells:
            c._row = self

    @property
    def cheight(self) -> int:
        return max([c.dheight for c in self.cells])

    @property
    def cwidth(self) -> int:
        return sum([c.dwidth for c in self.cells]) + self.width - 1

    @property
    def height(self) -> int:
        return 1

    @property
    def width(self) -> int:
        return len(self.cells)

    def fmt(self) -> list[str]:
        assert isinstance(self._table, Table)
        ch = self.cheight + self._table.hpadding
        lines = [''] * ch
        for ci in range(self.width):
            cw = self._table.column(ci).cwidth + self._table.wpadding
            lno = 0
            for cline in self.cells[ci].fmt(cw, ch):
                lines[lno] = lines[lno] + cline
                lno = lno + 1
        return [line.rstrip() for line in lines]


class Column:

    def __init__(self, cells: list[Cell]):
        self.cells = cells

    @property
    def cheight(self) -> int:
        return sum([c.dheight for c in self.cells])

    @property
    def cwidth(self) -> int:
        return max([c.dwidth for c in self.cells])

    @property
    def height(self) -> int:
        return len(self.cells)

    @property
    def width(self) -> int:
        return 1
