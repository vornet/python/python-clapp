#!/usr/bin/python3
"""
A simple application
"""

from __future__ import annotations

import abc
import os
import sys
import traceback

from typing import Any, Protocol, runtime_checkable

import clapp._meta
import clapp.err

from clapp.dbg import DEV, DevOpts


def _simple_items_fmt(L: list[tuple[str, Any]]) -> str:
    return ','.join([f'{k}={v}' for k, v in L])


def _simple_dict_fmt(D: dict) -> str:
    return ','.join([f'{k}={v}' for k, v in D.items()])


@runtime_checkable
class ActionCallbackP(Protocol):

    def __call__(self,
                 params: dict,
                 app: BaseApp,
                 ) -> int:
        ...


class _UsageMessage:

    @classmethod
    def from_exc(cls, app: Any, exc: Any):
        if type(exc) is clapp.err.MissingValueUsageError:
            return cls.from_missingval_exc(app, exc)
        if type(exc) is clapp.err.MissingArgumentUsageError:
            return cls.from_missingarg_exc(app, exc)
        if type(exc) is clapp.err.BadValueUsageError:
            return cls.from_badval_exc(app, exc)
        elif type(exc) is clapp.err.UnknownArgumentUsageError:
            return cls.from_unknown_exc(app, exc)
        elif type(exc) is clapp.err.UsageError:
            return cls.from_usage_exc(app, exc)
        raise ValueError(f"unsupported exception type: {exc!r}")

    @classmethod
    def from_unknown_exc(cls, app: Any,
                         exc: clapp.err.UnknownArgumentUsageError):
        detail = str(exc)
        if not detail:
            raise clapp.err.AppBug(
                "must provide argument when"
                f" raising UnknownArgumentUsageError: {exc}"
            )
        return cls(app, "unknown argument: " + detail)

    @classmethod
    def from_missingval_exc(cls, app: Any,
                            exc: clapp.err.MissingValueUsageError):
        detail = str(exc)
        if not detail:
            raise clapp.err.AppBug(
                "must provide value name when"
                f" raising MissingValueUsageError: {exc}"
            )
        return cls(app, "missing value for: " + detail)

    @classmethod
    def from_missingarg_exc(cls, app: Any,
                            exc: clapp.err.MissingArgumentUsageError):
        detail = str(exc)
        if not detail:
            raise clapp.err.AppBug(
                "must provide value name when"
                f" raising MissingArgumentUsageError: {exc}"
            )
        return cls(app, "missing positional argument: " + detail)

    @classmethod
    def from_badval_exc(cls, app: Any,
                        exc: clapp.err.BadValueUsageError):
        option_desc = exc.option_desc
        type_desc = exc.type_desc
        value = exc.value
        return f"bad format: {option_desc} must be {type_desc}; got {value!r}"

    @classmethod
    def from_usage_exc(cls, app: Any,
                       exc: clapp.err.UsageError):
        detail = str(exc)
        if not detail:
            return cls(app)
        return cls(app, detail)

    def __init__(self, app: Any, detail=None):
        self.app = app
        self.detail = detail

    @property
    def lines(self) -> list[str]:
        lines = self.app.base_usage
        if self.detail:
            lines.extend([''])
            lines.extend([self.detail])
        return lines

    @property
    def text(self) -> str:
        return '\n'.join(self.lines)


class Params(abc.ABC):

    _attrs_from_params: set = set()
    _params: dict = {}

    @classmethod
    def from_merge_of(cls, *sources: dict):
        merged: dict = {}
        for s in sources:
            merged.update(s)
        return cls(merged)

    def _setattrs_from(self, params: dict) -> set:
        did_set: set = set()
        subs: dict = {}
        for k, v in params.items():
            if k.startswith('.'):
                continue
            if k.endswith('.'):
                continue
            if '.' in k:
                head, rest = k.split('.', maxsplit=1)
                if not subs.get(head):
                    subs[head] = {}
                subs[head][rest] = v
                continue
            self.__set1attr(k, v)
        for sk, sv in subs.items():
            self.__set1attr(sk, Params(sv))
        return did_set

    def __set1attr(self, k: str, v: Any) -> None:
        setattr(self, k, v)
        self._attrs_from_params.add(k)

    def __init__(self, params: dict):
        self._params = params
        self._attrs_from_params = self._setattrs_from(params)
        self._setattrs_from(params)

    def __str__(self):
        cn = self.__class__.__name__
        params_desc = _simple_items_fmt([
            (k, getattr(self, k))
            for k in sorted(self._attrs_from_params)
        ])
        return f"{cn}({params_desc})"

    def __eq__(self, othr):
        them_mine = isinstance(othr, self.__class__)
        me_theirs = isinstance(self, othr.__class__)
        if them_mine or me_theirs:
            return self._params == othr._params
        return False
        raise ValueError(f"are not comparable: {type(self)} == {type(othr)}")


class Action(abc.ABC):

    def __init__(self, app: Any):
        self.app = app

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}(app={self.app})"

    def __repr__(self):
        return str(self)

    @abc.abstractmethod
    def do(self) -> int:
        return 0


def _no_action_cb(params: dict, app: BaseApp) -> int:
    cn = app.__class__.__name__
    raise clapp.err.AppBug(
        "could not determine action:\n"
        f"    self.default_action={app.default_action}\n"
        f"    self.default_action_cb={app.default_action_cb}\n"
        f"    self.params={app.params}\n"
        "  hint: a sub-class of clapp.Action' is either produced\n"
        f"  hint: by parsing {cn}.arg_scheme or provided by\n"
        f"  hint: property {cn}.default_action\n"
    )


class _NoActionAction(Action):

    def do(self):
        cn = self.app.__class__.__name__
        raise clapp.err.AppBug(
            "could not determine action:\n"
            f"    self.default_action={self.default_action}\n"
            f"    self.default_action_cb={self.default_action_cb}\n"
            f"    self.params={self.params}\n"
            "  hint: a sub-class of clapp.Action' is either produced\n"
            f"  hint: by parsing {cn}.arg_scheme or provided by\n"
            f"  hint: property {cn}.default_action\n"
        )
        return 5


def _show_version_a(params: dict, app: BaseApp) -> int:
    print(app.version)
    return 0


def _show_help_a(params: dict, app: BaseApp) -> int:
    app.show_help()
    return 0


class ShowVersionAction(Action):

    def do(self):
        print(self.app.version)
        return 0


class ShowHelpAction(Action):

    def do(self):
        self.app.show_help()
        return 0


class BaseApp(abc.ABC):
    """
    Main application class
    """

    dev_opts: DevOpts = DEV.dev_opts
    version: str = '0.0.0+_no_version_'

    @classmethod
    def main(cls, factory=None):
        """
        Main entry point.

        Initialize application object using *factory* or default factory
        (BaseApp.from_argv), run its run() method, collect exit status
        and exit with it.

        If run() fails to return usable exit status (int between 0 to
        255), warn about this and exit with 4.

        If run() throws clapp.err.UsageError or clapp.err.AppError, print the
        corresponding error to stderr and exit with corresponding exit status:
        2 and 3, respectively.

        If run() or the *factory* throw any other exception, print it to
        stderr and exit with 4.
        """
        def is_valid_es(X):
            return type(X) is int and X >= 0 and X < 256
        factory = factory if factory else cls.from_argv
        try:
            app = factory()
        except Exception:
            sys.stderr.write(traceback.format_exc())
            sys.exit(4)
        try:
            es = app.run()
        except clapp.err.HelpRequested as e:
            print(e)
            sys.exit(0)
        except clapp.err.UsageError as e:
            msg = _UsageMessage.from_exc(app, e)
            print(msg.text, file=sys.stderr)
            sys.exit(2)
        except clapp.err.AppError as e:
            print(e, file=sys.stderr)
            sys.exit(3)
        except Exception:
            sys.stderr.write(traceback.format_exc())
            try:
                diag_info = app.diag_info
                sys.stderr.write(f'diagnostic information: {diag_info}\n')
            except Exception:
                pass
            sys.exit(4)
        if type(es) is not int:
            msg = (
                "%s.run() did not return proper exit status: %r is not int"
                % (app.__class__.__name__, es)
            )
            BaseApp.WARN(msg)
            es = 4
        sys.exit(es)

    @classmethod
    def from_argv(cls):
        """
        Initialize BaseApp from passed arguments.

        First argument will become application name, rest will become
        application arguments.
        """
        return cls(
            name=sys.argv[0],
            args=sys.argv[1:],
        )

    def __init__(self, name: str, args: list[str]):
        self._name = name
        self.args = args
        self.params: dict = {}

    def __str__(self):
        cn = self.__class__.__name__
        return f"{cn}(args={self.args})"

    def __repr__(self):
        return str(self)

    def show_help(self) -> None:
        text = ''
        for line in self.help_lines:
            text += line + '\n'
        raise clapp.err.HelpRequested(text)

    @property
    def name(self) -> str:
        """
        Name used in help and usage messages.
        """
        return self._name

    @property
    def diag_info(self) -> dict:
        return {
            'clapp.version': clapp._meta.VERSION,
            'app.version': self.version,
            'app.name': self.name,
        }

    @property
    def base_usage(self) -> list[str]:
        """
        Build lines of basic usage error from BaseApp.usage_patterns.

        See BaseApp.usage_patterns for syntax.

        You can use this method when implementing BaseApp.help_lines
        to generate part of the help that describes syntax of your
        program.
        """
        patterns = self.usage_patterns
        if type(patterns) is str:
            return [f"usage: {patterns}"]
        if type(patterns) is list:
            if len(patterns) == 1:
                return [f"usage: {self.name} {patterns[0]}"]
            else:
                return (
                    ["usage:"]
                    + [f"  {self.name} {p}" for p in patterns]
                )
        raise ValueError(
            f"patterns must be str or list, not {type(patterns)}"
        )

    def throw_usage(self,
                    error: str | None = None,
                    unknown: str | None = None,
                    missingval: str | None = None,
                    missingarg: str | None = None,
                    badval: tuple[str, str, Any] | None = None,
                    ) -> None:
        """
        Throw clapp.err.UsageError with specific *error*.

        This method will take lines from BaseApp.base_usage and raise
        them as clapp.err.UsageError, causing the app to exit with appropriate
        status.

        See also BaseApp.base_usage and BaseApp.usage_patterns.

        This method also allows the message to be tailored to be more
        specific and thus potentially more helpful to user.  This is
        done by passing extra messages via *error*, *unknown* and
        *missingval*.

        *error* is a message providing specific hint such as what
        exactly is missing or which argument is unknown.  If
        provided, the message will be printed after the usage
        hint.

        *unknown*, *missingval*, *missingarg* and *badval* act similar as
        *error*, except that they produce specific error message:

         *  *unknown* describes unknown argument,
         *  *missingval* describes parameter with a missing value
            part,
         *  *missingarg* describes missing positional argument,
         *  and *badval* describes incorrect format of a parameter
            value.

        Most of the arguments accept simple strings that typically mention
        given offending value or argument name.

        *badval* requires more information so it must be tuple with three
        parts:

         1. human-readable name of the parameter,
         2. human-readable description of the expected format or type,
         3. offending value.
        """
        given: dict[str, Any] = {}
        if error:
            given['error'] = error
        if missingval:
            given['missingval'] = missingval
        if missingarg:
            given['missingarg'] = missingarg
        if badval:
            given['badval'] = badval
        if unknown:
            given['unknown'] = unknown
        if len(given.keys()) > 1:
            raise ValueError(f"cannot mix multiple types: {set(given.keys())}")
        if error:
            raise clapp.err.UsageError(error)
        if missingval:
            raise clapp.err.MissingValueUsageError(missingval)
        if missingarg:
            raise clapp.err.MissingArgumentUsageError(missingval)
        if badval:
            raise clapp.err.BadValueUsageError(
                option_desc=badval[0],
                type_desc=badval[1],
                value=badval[2],
            )
        if unknown:
            raise clapp.err.UnknownArgumentUsageError(unknown)

    @staticmethod
    def WARN(msg: str | list[str]):
        """
        Print *msg* (a string or a list of lines) to stderr.
        """
        if type(msg) is list:
            lines = msg
        else:
            lines = [str(msg)]
        for line in lines:
            print(line, file=sys.stderr)

    def _find_action_class(self) -> type:
        """
        Find and return clapp.Action sub-class to run
        """
        acls = self.params.get('clapp.Action', self.default_action)
        if not acls:
            cn = self.__class__.__name__
            raise clapp.err.AppBug(
                "could not determine action:\n"
                f"    self.default_action={self.default_action}\n"
                f"    self.default_action_cb={self.default_action_cb}\n"
                f"    self.params={self.params}\n"
                "  hint: a sub-class of clapp.Action' is either produced\n"
                f"  hint: by parsing {cn}.arg_scheme or provided by\n"
                f"  hint: property {cn}.default_action_cb\n"
            )
        if not isinstance(acls, type):
            raise clapp.err.AppBug(
                f"action must be sub-class of clapp.Action: {acls}"
            )
        if not issubclass(acls, Action):
            raise clapp.err.AppBug(
                f"action must be sub-class of clapp.Action: {acls}"
            )
        return acls

    def _find_action_callback(self) -> ActionCallbackP | None:

        def from_params() -> ActionCallbackP | None:
            fn = self.params.get('clapp.action')
            if fn is None:
                return None
            if not isinstance(fn, ActionCallbackP):
                raise clapp.err.AppBug(
                    f"action function must follow clapp.ActionCallbackP protocol: {fn}"
                )
            return fn

        def from_default() -> ActionCallbackP | None:
            fn = self.default_action_cb
            if not isinstance(fn, ActionCallbackP):
                return None
            return fn

        return from_params() or from_default() or None

    @property
    def debug(self) -> bool:
        return os.environ.get('CLAPP_DEBUG') == 'true'

    def run(self) -> int:
        """
        Run the application's action; return integer exit status
        """
        from_args = self.arg_scheme.parse_all(self.args)
        DEV.VAL(self, 'from_args', from_args)
        if not from_args:
            raise clapp.err.UsageError()
        try:
            self.params = self.init_params(from_args)
            afn = self._find_action_callback()
            if afn:
                return afn(params=self.params, app=self)
            DEV.DEPRECATION(
                app=self,
                msg="clapp.Action class is deprecated, use callback instead",
                hints=[
                    "in arg_scheme, instead of setting 'clapp.Action': SomeAction,",
                    "set 'clapp.action': callback, where callback is a callable which",
                    "accepts *params: dict* and *app: BaseApp*.",
                ],
            )
            acls = self._find_action_class()
            DEV.VAL(self, 'acls', acls)
            action = acls(self)
            DEV.VAL(self, 'action', action)
            return action.do()
        except Exception as exc:
            new_es, errors = self.handle_exception(exc=exc)
            if errors:
                BaseApp.WARN(errors)
            return 4 if new_es is None else new_es

    def handle_exception(self, exc: Exception) -> tuple[int | None, list[str] | str | None]:
        """
        Try and handle known exception *exc*

        Override this method to add custom exception handling on application
        level.  If overrided, the method must return a tuple containing
        two items (each of which can be None):

         *  Suggested exit status (integer from 0 to 255).  I

            If provided, clapp will exit with it instead of the default of 4.

         *  Error string (or list of thereof).

            If errors are provided, clapp will use BaseApp.WARN to print them
            errors to user.

        Any exceptions raised from this method will be handled as described in
        BaseApp.main(), including the special handling of AppError and
        UsageError.
        """
        raise exc

    def init_params(self, from_args: dict) -> dict:
        """
        Load and merge all sources of params to already parsed *from_args*

        *from_args* is a dictionary containing params obtained by parsing
        command line arguments using App.arg_scheme.  By default, this
        dictionary is left untouched and merely assigned to App.params.

        Implementing this function allows you to add or override parameters
        based on other sources such as file-based configuration or environment
        variables.
        """
        return from_args

    @property
    @abc.abstractmethod
    def help_lines(self) -> list[str]:
        """
        List of lines that should be shown as general help.
        """
        pass

    @property
    @abc.abstractmethod
    def arg_scheme(self) -> Any:
        """
        Instance of clapp.cli.Pattern
        """
        pass

    @property
    def default_action(self) -> type:
        """
        Instance of clapp.Action to do when arg_scheme does not produce one.
        """
        return _NoActionAction

    @property
    def default_action_cb(self) -> ActionCallbackP | None:
        """
        Callback function if arg_scheme does not produce one.
        """
        return None

    @property
    @abc.abstractmethod
    def usage_patterns(self) -> list[str]:
        """
        List of supported usage patterns.

        A list of strings, each describing a valid usage pattern, i.e.
        list of arguments that can appear in a single call together.
        Patterns specified here DON'T contain the 'usage: ' prefix
        nor the name of the program.

        For example:

            ['[--json] PATH']

        describes a simple CLI, which mandates PATH argument and
        supports optional argument '--json'.  Assuming that name
        of the application (passed when initializing BaseApp or
        detected from sys.argv) is 'myapp', the above would lead
        to usage error like this:

            usage: myapp [--json] PATH

        A more complex example describes set of sub-command based
        usage patterns:

            [
                'wait TIME',
                'reset',
                'open [FILE]',
            ]

        Corresponding usage error would be formatted like this:

            usage: myapp wait TIME
            usage: myapp reset
            usage: myapp open [FILE]

        """
        pass
