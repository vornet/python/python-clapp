class TableError(IndexError):
    pass


class _Padding:

    def __init__(self, horizontal, vertical):
        self.horizontal = horizontal
        self.vertical = vertical


class _SheetField:

    @classmethod
    def from_data(cls, data):
        def ex_err():
            return f'invalid node: not a tuple of (name, value): {data!r}'
        if not isinstance(data, tuple):
            raise ValueError(ex_err())
        if not len(data) == 2:
            raise ValueError(ex_err())
        return cls(name=data[0], value=data[1])

    def __init__(self, name, value):
        self.name = name
        self.value = value
        self._sheet = None
        self._sheet_line = None


class _SheetLine:

    @classmethod
    def from_data(cls, data):
        if not isinstance(data[1], list):
            single_field = _SheetField(name=data[0], value=data[1])
            return cls('_', [single_field])
        return cls(
            name=data[0],
            fields=[_SheetField.from_data(n) for n in data[1]]
        )

    def __init__(self, name, fields):
        self.name = name
        self.fields = fields
        for f in fields:
            f._sheet_line = self


class _SheetTextField(_SheetField):
    pass


class Sheet:

    @staticmethod
    def _check_node(node):
        def ex_err():
            return f'invalid node: not a tuple of (name, value): {node!r}'
        if not isinstance(node, tuple):
            raise ValueError(ex_err())
        if not len(node) == 2:
            raise ValueError(ex_err())

    @classmethod
    def from_data(cls, tree):
        lines = []
        for node in tree:
            cls._check_node(node)
            lines.append(_SheetLine.from_data(node))
        return cls(lines)

    def __init__(self, lines):
        self.lines = lines
        for ln in lines:
            ln._sheet = self

    def _fmt_line(self, line, tabstop):
        togo = line.fields.copy()
        words = []
        while togo:
            field = togo.pop(0)
            word = f'{field.name}: {field.value}'
            if togo:
                words.append((word + ' ').ljust(tabstop))
            else:
                words.append(word)
        return words

    def fmt(self, tabstop=8):
        return [
            ''.join(self._fmt_line(line, tabstop=tabstop))
            for line in self.lines
        ]


class Table:
    """
    Hold tabular data and allow formatting into plain-text.

    Adjusts width/height automatically based on number of columns and
    rows in the data.

    Allows specifying horizontal and vertical padding.
    """

    @classmethod
    def from_data(self, rows, hpadding=1, vpadding=0):
        trows = []
        for r in rows:
            trows.append(_TableRow([
                _TableCell(c) for c
                in r
            ]))
        return Table(trows, hpadding, vpadding)

    def __init__(self, rows, hpadding=1, vpadding=0):
        self.rows = rows
        for r in self.rows:
            r._table = self
        self.padding = _Padding(hpadding, vpadding)

    @property
    def width(self):
        return max([r.width for r in self.rows])

    @property
    def height(self):
        return len(self.rows)

    def column(self, n):
        cells = []
        if n >= self.width:
            raise TableError('no such column: %s' % n)
        for row in self.rows:
            try:
                cell = row.cells[n]
            except IndexError:
                cell = _TableCell()
            cells.append(cell)
        return _TableColumn(cells)

    def columns(self):
        columns = []
        for n in range(0, self.width - 1):
            columns.append(self.column(n))
        return columns

    def row(self, n):
        try:
            row = self.rows[n]
        except IndexError:
            raise TableError("no such row: %s" % n)
        return row

    def fmt(self):
        lines = []
        for row in self.rows:
            lines.extend(row.fmt())
        return lines


class _TableCell:

    def __init__(self, data=None):
        self.data = data
        self._table_row = None

    def __str__(self):
        return str(self.data)

    @property
    def dheight(self):
        return len(str(self).splitlines())

    @property
    def dwidth(self):
        return len(str(self))

    @property
    def height(self):
        return 1

    @property
    def width(self):
        return 1

    def fmt(self, max_width, max_height):
        if max_width < self.dwidth:
            raise TableError('cannot fit into width: %d' % max_width)
        if max_height < self.dheight:
            raise TableError('cannot fit into height: %d' % max_height)
        lines = []
        togo = str(self).splitlines()
        while max_height:
            if togo:
                lines.append(('%%-%ds' % max_width) % togo.pop(0))
            else:
                lines.append('')
            max_height = max_height - 1
        return lines


class _TableRow:

    def __init__(self, cells):
        self.cells = cells
        self._table = None
        for c in self.cells:
            c._table_row = self

    @property
    def cheight(self):
        return max([c.dheight for c in self.cells])

    @property
    def cwidth(self):
        return sum([c.dwidth for c in self.cells]) + self.width - 1

    @property
    def height(self):
        return 1

    @property
    def width(self):
        return len(self.cells)

    def fmt(self):
        ch = self.cheight + self._table.padding.vertical
        lines = [''] * ch
        for ci in range(self.width):
            cw = self._table.column(ci).cwidth + self._table.padding.horizontal
            lno = 0
            for cline in self.cells[ci].fmt(cw, ch):
                lines[lno] = lines[lno] + cline
                lno = lno + 1
        return lines


class _TableColumn:

    def __init__(self, cells):
        self.cells = cells

    @property
    def cheight(self):
        return sum([c.dheight for c in self.cells])

    @property
    def cwidth(self):
        return max([c.dwidth for c in self.cells])

    @property
    def height(self):
        return len(self.cells)

    @property
    def width(self):
        return 1
