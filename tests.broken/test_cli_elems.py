from tests import fw

import clapp.cli


def mkexc_ve(data):
    return {
        'exc': 'ValueError',
        'exc_data': data
    }


class FactoryTestDriver(fw.ObjTestDriver):

    def make_object(self, *args, **kwargs):
        fac_name = args[0]
        fac_arg = args[1]
        fac_fn = getattr(clapp.cli, fac_name)
        return fac_fn(fac_arg)

    def is_expectable(self, e):
        return e.__class__.__name__ in [
            'ValueError',
        ]


class ScalarFactoriesTest(fw.SimpleObjHooverTest):

    result_driver = FactoryTestDriver
    oracle_driver = fw.FakeObjOracleDriver

    def run_with(self, fac_name, fac_arg, oracle=None):
        self.args = [fac_name, fac_arg]
        self.oracle = oracle or {'class': 'str'}
        self.autorun()

    #
    # F_ANY
    #

    def test_any_ok(self):
        self.run_with(
            'F_ANY', '1',
        )

    def test_any_empty(self):
        self.run_with(
            'F_ANY', '',
        )

    def test_any_spaces(self):
        self.run_with(
            'F_ANY', ' ',
        )

    #
    # F_ID
    #

    def test_id_ok(self):
        self.run_with(
            'F_ID', 'foo',
        )

    def test_id_busy(self):
        self.run_with(
            'F_ID', 'foo_the133ThingstoRemember',
        )

    def test_id_1char(self):
        self.run_with(
            'F_ID', 'f',
        )

    def test_id_1cap(self):
        self.run_with(
            'F_ID', 'F',
        )

    def test_id_1under(self):
        self.run_with(
            'F_ID', '_',
        )

    def test_id_allchars(self):
        self.run_with(
            'F_ID', 'foo',
        )

    def test_id_allcaps(self):
        self.run_with(
            'F_ID', 'FOO',
        )

    def test_id_allunders(self):
        self.run_with(
            'F_ID', '___',
        )

    def test_id_empty(self):
        self.run_with(
            'F_ID', '',
            mkexc_ve("not a valid ID: ''"),
        )

    def test_id_digit(self):
        self.run_with(
            'F_ID', '1foo',
            mkexc_ve("not a valid ID: '1foo'"),
        )

    def test_id_dash(self):
        self.run_with(
            'F_ID', 'fo-o',
            mkexc_ve("not a valid ID: 'fo-o'"),
        )

    def test_id_dashbgn(self):
        self.run_with(
            'F_ID', '-foo',
            mkexc_ve("not a valid ID: '-foo'"),
        )

    def test_id_dashend(self):
        self.run_with(
            'F_ID', 'foo-',
            mkexc_ve("not a valid ID: 'foo-'"),
        )

    def test_id_dot(self):
        self.run_with(
            'F_ID', 'fo.o',
            mkexc_ve("not a valid ID: 'fo.o'"),
        )

    def test_id_dotbgn(self):
        self.run_with(
            'F_ID', '.foo',
            mkexc_ve("not a valid ID: '.foo'"),
        )

    def test_id_dotend(self):
        self.run_with(
            'F_ID', 'foo.',
            mkexc_ve("not a valid ID: 'foo.'"),
        )

    def test_id_colon(self):
        self.run_with(
            'F_ID', 'fo:o',
            mkexc_ve("not a valid ID: 'fo:o'"),
        )

    def test_id_colonbgn(self):
        self.run_with(
            'F_ID', ':foo',
            mkexc_ve("not a valid ID: ':foo'"),
        )

    def test_id_colonend(self):
        self.run_with(
            'F_ID', 'foo:',
            mkexc_ve("not a valid ID: 'foo:'"),
        )

    #
    # F_KEY
    #

    def test_key_ok(self):
        self.run_with(
            'F_KEY', 'foo',
        )

    def test_key_busy(self):
        self.run_with(
            'F_KEY', 'foo_the133ThingstoRemember',
        )

    def test_key_1char(self):
        self.run_with(
            'F_KEY', 'f',
        )

    def test_key_1cap(self):
        self.run_with(
            'F_KEY', 'F',
        )

    def test_ke_keyer(self):
        self.run_with(
            'F_KEY', '_',
        )

    def test_key_allchars(self):
        self.run_with(
            'F_KEY', 'foo',
        )

    def test_key_allcaps(self):
        self.run_with(
            'F_KEY', 'FOO',
        )

    def test_key_allunders(self):
        self.run_with(
            'F_KEY', '___',
        )

    def test_key_digit(self):
        self.run_with(
            'F_KEY', '1foo',
        )

    def test_key_dash(self):
        self.run_with(
            'F_KEY', 'fo-o',
        )

    def test_key_dashbgn(self):
        self.run_with(
            'F_KEY', '-foo',
        )

    def test_key_dashend(self):
        self.run_with(
            'F_KEY', 'foo-',
        )

    def test_key_dot(self):
        self.run_with(
            'F_KEY', 'fo.o',
        )

    def test_key_dotbgn(self):
        self.run_with(
            'F_KEY', '.foo',
        )

    def test_key_dotend(self):
        self.run_with(
            'F_KEY', 'foo.',
        )

    def test_key_colon(self):
        self.run_with(
            'F_KEY', 'fo:o',
        )

    def test_key_colonbgn(self):
        self.run_with(
            'F_KEY', ':foo',
        )

    def test_key_colonend(self):
        self.run_with(
            'F_KEY', 'foo:',
        )

    def test_key_empty(self):
        self.run_with(
            'F_KEY', '',
            mkexc_ve("not a valid KEY: ''"),
        )

    #
    # F_KVMAP
    #

    def test_kvmap_empty(self):
        self.run_with(
            'F_KVMAP', '',
            {'class': 'dict'}
        )

    def test_kvmap_one(self):
        self.run_with(
            'F_KVMAP', 'foo=bar',
            {'class': 'dict'}
        )

    def test_kvmap_bool(self):
        self.run_with(
            'F_KVMAP', 'foo',
            {'class': 'dict'}
        )

    def test_kvmap_few(self):
        self.run_with(
            'F_KVMAP', 'foo=bar,baz=Baz,quux=yay',
            {'class': 'dict'}
        )

    def test_kvmap_repeat(self):
        self.run_with(
            'F_KVMAP', 'foo=bar,baz=Baz,foo=yay',
            {'class': 'dict'}
        )

    def test_kvmap_repeat_bool(self):
        self.run_with(
            'F_KVMAP', 'foo=bar,baz=Baz,foo',
            {'class': 'dict'}
        )

    def test_kvmap_digitkey(self):
        self.run_with(
            'F_KVMAP', 'foo=bar,1=value',
            {'class': 'dict'}
        )

    def test_kvmap_emptykey(self):
        self.run_with(
            'F_KVMAP', 'foo=bar,=value',
            mkexc_ve("not a valid KEY: ''"),
        )

    #
    # F_NONEMPTY
    #

    def test_nonempty_ok(self):
        self.run_with(
            'F_NONEMPTY', 'foo',
        )

    def test_nonempty_spaces(self):
        self.run_with(
            'F_NONEMPTY', '  ',
        )

    def test_nonempty_empty(self):
        self.run_with(
            'F_NONEMPTY', '',
            mkexc_ve("not a valid value: '' is empty"),
        )

    #
    # F_POSINT
    #

    def test_posint_ok(self):
        self.run_with(
            'F_POSINT', '1',
            {'class': 'int'}
        )

    def test_posint_with_spaces(self):
        self.run_with(
            'F_POSINT', ' 1	',
            {'class': 'int'}
        )

    def test_posint_negative(self):
        self.run_with(
            'F_POSINT', '-1',
            mkexc_ve("not a positive integer: -1"),
        )

    def test_posint_empty(self):
        self.run_with(
            'F_POSINT', '',
            mkexc_ve("not an integer: ''"),
        )

    def test_posint_spaceonly(self):
        self.run_with(
            'F_POSINT', ' ',
            mkexc_ve("not an integer: ' '"),
        )

    def test_posint_badsyn(self):
        self.run_with(
            'F_POSINT', 'z',
            mkexc_ve("not an integer: 'z'"),
        )


# _BUILTIN_CONDITIONS: set[Callable] = {
#     C_ANY,
#     C_GNU_LONG,
#     C_GNU_SHORT,
#     C_GNU_TERMINATOR,
#     C_NONESUCH,
#     C_NONOPT,
#     C_UNIX_ABSPATH,
#     C_UNIX_PATH,
#     C_XORG_LONG,
# }
#
#
