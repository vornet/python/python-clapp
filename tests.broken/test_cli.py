import unittest

import hoover

from tests import fw

import clapp.cli


class BoolSpecOracleDriver(fw.SpecOracleDriver):

    @staticmethod
    def make_oracle(args):

        def just_some_foos(args):
            return set(args) == set(['--foo'])

        def first_non_foo(args):
            return [a for a in args if a != '--foo'][0]

        if not args:
            return {'params': {}}
        if just_some_foos(args):
            return {'params': {'Foo': True}}
        return fw.mkexc_r('UnknownArgumentUsageError', first_non_foo(args))


class BoolTest(fw.CartmanHooverTest):

    scheme = {
        'spec': hoover.Cartman.Scalar,
        'args': hoover.Cartman.Iterable,
    }

    argsrc = {
        'spec': clapp.cli.Pattern(
            options={
                'Foo': clapp.cli.Bool('--foo'),
            },
        ),
        'args': fw.enum_argsets(),
    }

    oracle_driver = BoolSpecOracleDriver


class EnumSpecOracleDriver(fw.SpecOracleDriver):

    @staticmethod
    def make_oracle(args):

        def means_foo_m(arg):
            return arg in ['-f', '--foo']

        def means_bar_m(arg):
            return arg in ['-b', '--bar']

        def is_known(arg):
            return means_foo_m(arg) or means_bar_m(arg)

        params = {}
        if not args:
            params = {}
        for arg in args:
            if not is_known(arg):
                return fw.mkexc_r('UnknownArgumentUsageError', arg)
            if means_foo_m(arg):
                params = {'Mode': 'foo_m'}
            if means_bar_m(arg):
                params = {'Mode': 'bar_m'}
        return {'params': params}


class EnumTest(fw.CartmanHooverTest):

    scheme = {
        'spec': hoover.Cartman.Scalar,
        'args': hoover.Cartman.Iterable,
    }

    argsrc = {
        'spec': clapp.cli.Pattern(
            options={
                'Mode': clapp.cli.Enum({
                    '-f|--foo': 'foo_m',
                    '-b|--bar': 'bar_m',
                }),
            },
        ),
        'args': fw.enum_argsets(),
    }

    oracle_driver = EnumSpecOracleDriver


class KeyValueSpecOracleDriver(fw.SpecOracleDriver):

    @staticmethod
    def make_oracle(args):
        """
        Load every pair of '--foo' and the next argument
        to dict and return the dict and a boolean indicating
        whether '--foo' was left "hungry" at the end.
        """
        missing_val = None
        params = {}
        togo = args.copy()
        while togo:
            arg = togo.pop(0)
            if missing_val:
                params['Foo'] = arg
                missing_val = None
            elif arg == '--foo':
                missing_val = arg
            else:
                return fw.mkexc_r('UnknownArgumentUsageError', arg)
        if missing_val is not None:
            return fw.mkexc_s('MissingValueUsageError', 'KeyValue(--foo)')
        return {'params': params}


class KeyValueTest(fw.CartmanHooverTest):

    scheme = {
        'spec': hoover.Cartman.Scalar,
        'args': hoover.Cartman.Iterable,
    }

    argsrc = {
        'spec': clapp.cli.Pattern(
            options={
                'Foo': clapp.cli.KeyValue('--foo', str),
            },
        ),
        'args': fw.enum_argsets(),
    }

    oracle_driver = KeyValueSpecOracleDriver


class EnumPosSpecOracleDriver(fw.SpecOracleDriver):

    @staticmethod
    def make_oracle(args):
        known = {'foo': 'FOO-ACTION', 'bar': 'BAR-ACTION'}
        togo = args.copy()
        if not togo:
            return fw.mkexc_s(
                'MissingArgumentUsageError',
                'EnumPos(foo|bar)'
            )
        if togo[0] in known and len(togo) == 1:
            # optimal usage
            return {'params': {'Action': known[togo[0]]}}
        if togo[0] in known and len(togo) > 1:
            # we don't know the next one so exception is imminent
            togo.pop(0)
        return fw.mkexc_r('UnknownArgumentUsageError', togo[0])


class EnumPosTest(fw.CartmanHooverTest):

    scheme = {
        'spec': hoover.Cartman.Scalar,
        'args': hoover.Cartman.Iterable,
    }

    argsrc = {
        'spec': clapp.cli.Pattern(
            positionals=[
                ('Action', clapp.cli.EnumPos({
                    'foo': 'FOO-ACTION',
                    'bar': 'BAR-ACTION',
                })),
            ],
        ),
        'args': fw.enum_argsets(),
    }

    oracle_driver = EnumPosSpecOracleDriver


class ListPosSpecOracleDriver(fw.SpecOracleDriver):

    @staticmethod
    def make_oracle(args):
        """
        Just return the args.

        The list could be any number of elements long including empty and
        there are no constraints on individual elements, so there's nothing to
        complain about.
        """
        return {'params': {'Files': args}}


class ListPosTest(fw.CartmanHooverTest):

    scheme = {
        'spec': hoover.Cartman.Scalar,
        'args': hoover.Cartman.Iterable,
    }

    argsrc = {
        'spec': clapp.cli.Pattern(
            positionals=[
                ('Files', clapp.cli.ListPos()),
            ],
        ),
        'args': fw.enum_argsets(),
    }

    oracle_driver = ListPosSpecOracleDriver


test_cases = (
    BoolTest,
    EnumTest,
    KeyValueTest,
    EnumPosTest,
    ListPosTest,
)


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    for tc in test_cases:
        tests = loader.loadTestsFromTestCase(tc)
        suite.addTests(tests)
    return suite
