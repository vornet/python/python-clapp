import unittest

from tests import fw

from clapp.cli import (
    Bool,
    C_NONOPT,
    F_NONEMPTY,
    EnumPos,
    KeyValue,
    ListPos,
    LiteralPos,
    NonemptyListPos,
    MaybeScalarPos,
    Pattern,
    ScalarPos,
    P_VERSION,
    P_HELP,
)


class JustFooTest(fw.SimpleHooverTest):

    spec = Pattern({'Foo': KeyValue('--foo')})

    def test_foo(self):
        self.args = ['--foo', '--foo']
        self.oracle = {
            'params': {
                'Foo': '--foo',
            },
        }
        self.autorun()


class OtherappTest(fw.SimpleHooverTest):

    spec = Pattern(
        options={
            'Size': KeyValue('--size', int, trigger={'Action': 'print'}),
        },
        positionals=[(
            'Action', EnumPos({
                '--print': 'print',
                '--fail': 'fail',
                '--fail-unexpectedly': 'fail_unexpectedly',
                '--warn': 'warn',
                '--badret': 'badret',
                '--version': 'version',
                '--help': 'help',
            })
        )],
    )

    def test_foo(self):
        self.args = ['--size', '9', '--print']
        self.oracle = {
            'params': {
                'Size': 9,
                'Action': 'print',
            },
        }
        self.autorun()


class FooSizeDryTest(fw.SimpleHooverTest):

    spec = Pattern(
        options={
            'Foo': KeyValue('--foo', factory=str),
            'SIZE': KeyValue('--size', factory=int),
            'Dry': Bool('--dry', '--no-dry', default=False),
        },
        positionals=(
            ('Path', MaybeScalarPos(factory=F_NONEMPTY)),
            ('Src', ScalarPos()),
            ('Dst', ScalarPos()),
            ('Items', ListPos()),
        ),
    )

    def test_all(self):
        self.args = [
            '--foo', 'bar',
            '--dry',
            '--size', '9',
            '/home/netvor',
            'john', 'jane',
            'mallory',
        ]
        self.oracle = {
            'params': {
                'SIZE': 9,
                'Dry': True,
                'Foo': 'bar',
                'Src': 'john',
                'Dst': 'jane',
                'Items': ["mallory"],
                'Path': '/home/netvor',
            },
        }
        self.autorun()


class SubpatternAppTest(fw.SimpleHooverTest):

    @property
    def spec(self):
        """
        Spec is a bit more complex.

        app [common-options] add [add-options] JOB PERSON
        app [common-options] rm [rm-options] JOB
        app [common-options] ls [ls-options]
        app --version
        app --help
        """
        common_options = {
            'Config': KeyValue('-c|--config'),
            'Debug': Bool('-d|--debug'),
        }
        S = Pattern()
        S.add_pattern(P_VERSION)
        S.add_pattern(P_HELP)
        S.add_pattern(Pattern(
            options=common_options,
            positionals=[
                ('_', LiteralPos('add')),
                ('SUBCOMMAND_PARAMS', Pattern(
                    options={
                        'Date': KeyValue('-d|--date'),
                    },
                    positionals=[
                        ('JOB', ScalarPos()),
                        ('PERSON', ScalarPos()),
                    ]
                ))
            ],
        ))
        S.add_pattern(Pattern(
            options=common_options,
            positionals=[
                ('_', LiteralPos('rm')),
                ('SUBCOMMAND_PARAMS', Pattern(
                    options={
                        'Force': Bool('-f|--force'),
                    },
                    positionals=[
                        ('JOB', ScalarPos()),
                    ]
                ))
            ],
        ))
        S.add_pattern(Pattern(
            options=common_options,
            positionals=[
                ('_', LiteralPos('ls')),
                ('SUBCOMMAND_PARAMS', Pattern(
                    options={
                        'All': Bool('-a|--all'),
                    },
                ))
            ],
        ))
        return S

    def mkoracle_exc(self):
        """
        Make oracle data for exception.

        All exceptions look the same; text is always just repeating
        the argument list, so there's no need to repeat it every time
        there's an expectation of exception.
        """
        return {
            "exc": "UsageError",
            "exc_data": f"no usage pattern matched: {self.args}",
        }

    def test_add_max_ok(self):
        self.args = [
            '--config', 'app.ini',
            '--debug',
            'add',
            '-d', '2022-01-02',
            'barista', 'Bob',
        ]
        self.oracle = {
            'params': {
                'Config': 'app.ini',
                'Debug': True,
                '_': 'add',
                'SUBCOMMAND_PARAMS': {
                    'Date': '2022-01-02',
                    'JOB': 'barista',
                    'PERSON': 'Bob',
                },
            },
        }
        self.autorun()

    def test_add_bad_common_option(self):
        self.args = [
            '--config', 'app.ini',
            '--debug',
            '--troll',
            'add',
            '-d', '2022-01-02',
            'barista', 'Bob',
        ]
        self.oracle = self.mkoracle_exc()
        self.autorun()

    def test_add_bad_local_option(self):
        self.args = [
            '--config', 'app.ini',
            '--debug',
            'add',
            '-d', '2022-01-02',
            '--troll',
            'barista', 'Bob',
        ]
        self.oracle = self.mkoracle_exc()
        self.autorun()

    def test_add_min_ok(self):
        self.args = [
            'add',
            'barista', 'Bob',
        ]
        self.oracle = {
            'params': {
                '_': 'add',
                'SUBCOMMAND_PARAMS': {
                    'JOB': 'barista',
                    'PERSON': 'Bob',
                },
            },
        }
        self.autorun()

    def test_add_trailing(self):
        self.args = [
            'add',
            'barista', 'Bob',
            'Trollson',
        ]
        self.oracle = self.mkoracle_exc()
        self.autorun()

    def test_add_missing_all(self):
        self.args = [
            'add',
        ]
        self.oracle = self.mkoracle_exc()
        self.autorun()

    def test_add_missing_person(self):
        self.args = [
            'add',
            'barista',
        ]
        self.oracle = self.mkoracle_exc()
        self.autorun()

    def test_rm_max_ok(self):
        self.args = [
            'rm',
            '-f',
            'barista'
        ]
        self.oracle = {
            'params': {
                '_': 'rm',
                'SUBCOMMAND_PARAMS': {
                    'Force': True,
                    'JOB': 'barista',
                },
            },
        }
        self.autorun()

    def test_rm_min_ok(self):
        self.args = [
            'rm',
            'barista'
        ]
        self.oracle = {
            'params': {
                '_': 'rm',
                'SUBCOMMAND_PARAMS': {
                    'JOB': 'barista',
                },
            },
        }
        self.autorun()

    def test_ls_max_ok(self):
        self.args = [
            'ls',
            '-a'
        ]
        self.oracle = {
            'params': {
                '_': 'ls',
                'SUBCOMMAND_PARAMS': {
                    'All': True,
                },
            },
        }
        self.autorun()

    def test_ls_min_ok(self):
        self.args = [
            'ls',
        ]
        self.oracle = {
            'params': {
                '_': 'ls',
                'SUBCOMMAND_PARAMS': {},
            },
        }
        self.autorun()

    def test_help_ok(self):
        self.args = [
            '--help',
        ]
        self.oracle = {
            'params': {
                "_": "--help",
                'clapp.Action': "<class 'clapp.app.ShowHelpAction'>",
            },
        }
        self.autorun()

    def test_help_trailing(self):
        self.args = [
            '--help',
            'me',
        ]
        self.oracle = self.mkoracle_exc()
        self.autorun()

    def test_help_leading(self):
        self.args = [
            'please',
            '--help',
        ]
        self.oracle = self.mkoracle_exc()
        self.autorun()

    def test_version_ok(self):
        self.args = [
            '--version',
        ]
        self.oracle = {
            'params': {
                "_": "--version",
                'clapp.Action': "<class 'clapp.app.ShowVersionAction'>",
            },
        }
        self.autorun()

    def test_noarg(self):
        self.args = []
        self.oracle = self.mkoracle_exc()
        self.autorun()
