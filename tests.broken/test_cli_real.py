import unittest

from tests import fw

from clapp.cli import (
    Bool,
    C_NONOPT,
    EnumPos,
    KeyValue,
    ListPos,
    LiteralPos,
    NonemptyListPos,
    Pattern,
    ScalarPos,
    _SP_HELP,
    _SP_VERSION,
)


class TikrCliTest(fw.SimpleHooverTest):

    @property
    def spec(self):
        opts = {'CONFIG_FILE': KeyValue('-c|--config-file')}
        S = Pattern()
        S.add_pattern(_SP_HELP)
        S.add_pattern(_SP_VERSION)
        S.add_pattern(Pattern(
            options=opts,
            positionals=[
                ('NAME', ScalarPos(C_NONOPT)),
            ],
            trigger={'clapp.Action': 'WaitTikrAction'},
        ))
        S.add_pattern(Pattern(
            options=opts,
            positionals=[
                ('_', LiteralPos('--')),
                ('NAME', ScalarPos()),
            ],
            trigger={'clapp.Action': 'WaitTikrAction'},
        ))
        S.add_pattern(Pattern(
            options=opts,
            positionals=[
                ('_', LiteralPos('-l|--list'))
            ],
            trigger={'clapp.Action': 'ListTikrsAction'},
        ))
        S.add_pattern(Pattern(
            options=opts,
            positionals=[
                ('_', LiteralPos('-e|--expression')),
                ('EXPRESSION', ScalarPos()),
            ],
            trigger={'clapp.Action': 'ExpressionTikrAction'},
        ))
        return S

    def test_noargs(self):
        self.args = []
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_run_tikr(self):
        self.args = ['foo']
        self.oracle = {
            'params': {
                'clapp.Action': 'WaitTikrAction',
                'NAME': 'foo',
            }
         }
        self.autorun()

    def test_run_tikr_named_list(self):
        self.args = ['--', '--list']
        self.oracle = {
            'params': {
                '_': '--',
                'clapp.Action': 'WaitTikrAction',
                'NAME': '--list',
            },
        }
        self.autorun()

    def test_unknown_arg(self):
        self.args = ['--fist']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_expr_missing(self):
        self.args = ['-e']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_name_missing(self):
        self.args = ['--']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_list(self):
        self.args = ['--list']
        self.oracle = {
            'params': {
                '_': '--list',
                'clapp.Action': 'ListTikrsAction',
            },
        }
        self.autorun()

    def test_list_extraarg(self):
        self.args = ['--list', 'foo']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_helparg(self):
        self.args = ['--help']
        self.oracle = {
            'params': {
                '_': '--help',
                'clapp.Action': 'clapp.ShowHelpAction',
            },
        }
        self.autorun()


class BmoDmenuCliTest(fw.SimpleHooverTest):

    spec = Pattern(
        options={
            'lines': KeyValue('-l|--lines', factory=int),
            'split_delim': KeyValue('--split', trigger={'split': True}),
            'split_field': KeyValue('--split-field', trigger={'split': True}),
        },
        patterns=[
            Pattern(
                positionals=[],
                trigger={'clapp.Action': 'RunAction'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('--')),
                    ('extra_args', ListPos()),
                ],
                trigger={'clapp.Action': 'RunAction'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('--help'))
                ],
                trigger={'clapp.Action': 'clapp.ShowHelpAction'},
            ),
        ],
    )

    def test_noargs(self):
        self.args = []
        self.oracle = {
            'params': {
                'clapp.Action': 'RunAction',
            }
         }
        self.autorun()

    def test_unknown_arg(self):
        self.args = ['--fist']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_extra_args(self):
        self.args = ['--', 'foo']
        self.oracle = {
            'params': {
                '_': '--',
                'clapp.Action': 'RunAction',
                'extra_args': self.args[1:],
            }
         }
        self.autorun()

    def test_lines(self):
        self.args = ['--lines', '30']
        self.oracle = {
            'params': {
                'clapp.Action': 'RunAction',
                'lines': 30,
            },
        }
        self.autorun()

    def test_lines_badval(self):
        self.args = ['--lines', '3z0']
        self.oracle = fw.mkexc_s(
            'BadValueUsageError',
            "('lines in KeyValue(-l|--lines)', 'int', '3z0')",
        )
        self.autorun()

    def test_lines_missingval(self):
        self.args = ['--lines']
        self.oracle = fw.mkexc_s(
            'MissingValueUsageError',
            "KeyValue(-l|--lines)",
        )
        self.autorun()

    def test_split(self):
        self.args = ['--split', '5']
        self.oracle = {
            'params': {
                'clapp.Action': 'RunAction',
                'split_delim': '5',
                'split': True,
            },
        }
        self.autorun()

    def test_split_missingval(self):
        self.args = ['--split']
        self.oracle = fw.mkexc_s(
            'MissingValueUsageError',
            "KeyValue(--split)",
        )
        self.autorun()

    def test_split_field(self):
        self.args = ['--split-field', '3-5']
        self.oracle = {
            'params': {
                'clapp.Action': 'RunAction',
                'split_field': '3-5',
                'split': True,
            },
        }
        self.autorun()

    def test_split_field_missingval(self):
        self.args = ['--split-field']
        self.oracle = fw.mkexc_s(
            'MissingValueUsageError',
            "KeyValue(--split-field)",
        )
        self.autorun()

    def test_all_options(self):
        self.args = ['--lines', '3000', '--split', '|', '--split-field', '3-']
        self.oracle = {
            'params': {
                'clapp.Action': 'RunAction',
                'lines': 3000,
                'split': True,
                'split_delim': '|',
                'split_field': '3-',
            }
        }
        self.autorun()

    def test_all_options_and_extras(self):
        self.args = [
            '--lines', '3000',
            '--split', '|',
            '--split-field', '3-',
            '--',
            '-p', 'hey:',
        ]
        self.oracle = {
            'params': {
                '_': '--',
                'clapp.Action': 'RunAction',
                'lines': 3000,
                'split': True,
                'split_delim': '|',
                'split_field': '3-',
                'extra_args': ['-p', 'hey:']
            }
        }
        self.autorun()

    def test_helparg(self):
        self.args = ['--help']
        self.oracle = {
            'params': {
                '_': '--help',
                'clapp.Action': 'clapp.ShowHelpAction',
            },
        }
        self.autorun()


class BmoMenuCliTest(fw.SimpleHooverTest):

    # usage:
    #   bmo menu -l|--list
    #   bmo menu [--open] NAME
    #   bmo menu --help

    spec = Pattern(
        patterns=[
            Pattern(
                positionals=[
                    ('NAME', ScalarPos(cond=C_NONOPT)),
                ],
                trigger={'clapp.Action': 'OpenMenuAction'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('--open')),
                    ('NAME', ScalarPos()),
                ],
                trigger={'clapp.Action': 'OpenMenuAction'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('-l|--list')),
                ],
                trigger={'clapp.Action': 'ListMenusAction'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('--help'))
                ],
                trigger={'clapp.Action': 'clapp.ShowHelpAction'},
            ),
        ]
    )

    def test_noargs(self):
        self.args = []
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_unknown_arg(self):
        self.args = ['--fist']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_list_menus(self):
        self.args = ['--list']
        self.oracle = {
            'params': {
                '_': '--list',
                'clapp.Action': 'ListMenusAction',
            }
         }
        self.autorun()

    def test_list_menus_z(self):
        self.args = ['--listz']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_list_menus_extraarg(self):
        self.args = ['--list', 'bar']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_run_menu(self):
        self.args = ['foo']
        self.oracle = {
            'params': {
                'clapp.Action': 'OpenMenuAction',
                'NAME': 'foo',
            }
         }
        self.autorun()

    def test_run_menu_extraarg(self):
        self.args = ['foo', 'bar']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_run_menu_named_list(self):
        self.args = ['--open', '--list']
        self.oracle = {
            'params': {
                '_': '--open',
                'clapp.Action': 'OpenMenuAction',
                'NAME': '--list',
            },
        }
        self.autorun()

    def test_helparg(self):
        self.args = ['--help']
        self.oracle = {
            'params': {
                '_': '--help',
                'clapp.Action': 'clapp.ShowHelpAction',
            },
        }
        self.autorun()

    def test_helparg_extraarg(self):
        self.args = ['--help', 'bar']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()


class ZkCliTest(fw.SimpleHooverTest):

    spec = Pattern(
        options={'Root': KeyValue('-r|--root')},
        patterns=[
            Pattern(
                positionals=[('_', LiteralPos('find'))],
                trigger={'clapp.Action': 'FindAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('find'))],
                trigger={'clapp.Action': 'DwimAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('mk'))],
                trigger={'clapp.Action': 'CreateNoteAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('open'))],
                trigger={'clapp.Action': 'OpenNoteAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('testf'))],
                trigger={'clapp.Action': 'TestNoteIdAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('path'))],
                trigger={'clapp.Action': 'ToPathAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('rm'))],
                trigger={'clapp.Action': 'RmNoteAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('chldrn'))],
                trigger={'clapp.Action': 'ListChildrenAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('xp'))],
                trigger={'clapp.Action': 'SendPathToCcAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('xr'))],
                trigger={'clapp.Action': 'SendRefToCcAction'},
            ),
            Pattern(
                positionals=[('_', LiteralPos('x'))],
                trigger={'clapp.Action': 'SendNoteToCcAction'},
            ),
        ],
        )

    def test_noargs(self):
        self.args = []
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_xp(self):
        self.args = ['xr']
        self.oracle = {
            'params': {
                '_': 'xr',
                'clapp.Action': 'SendRefToCcAction',
            }
        }
        self.autorun()

    def test_root_xp(self):
        self.args = ['-r', '/foo', 'xr']
        self.oracle = {
            'params': {
                '_': 'xr',
                'clapp.Action': 'SendRefToCcAction',
                'Root': '/foo',
            }
        }
        self.autorun()

    def test_root_twice_xp(self):
        self.args = ['-r', '/foo', '-r', '/bar', 'xr']
        self.oracle = {
            'params': {
                '_': 'xr',
                'clapp.Action': 'SendRefToCcAction',
                'Root': '/bar',
            }
        }
        self.autorun()

    def test_root_nopos(self):
        self.args = ['-r', '/foo']
        self.oracle = fw.mkexc_nupm([])     # this is weird but .. ok
        self.autorun()


class ZkEnumCliTest(fw.SimpleHooverTest):

    spec = Pattern(
        options={'Root': KeyValue('-r|--root')},
        positionals=[
            ('clapp.Action', EnumPos({
                'find': 'FindAction',
                'do': 'DwimAction',
                'mk': 'CreateNoteAction',
                'mk': 'CreateNoteAction',
                'mk': 'CreateNoteAction',
                'open': 'OpenNoteAction',
                'testf': 'TestNoteIdAction',
                'path': 'ToPathAction',
                'rm': 'RmNoteAction',
                'chldrn': 'ListChildrenAction',
                'xp': 'SendPathToCcAction',
                'xr': 'SendRefToCcAction',
                'x': 'SendNoteToCcAction',
            }))
        ]
    )

    def test_noargs(self):
        self.args = []
        self.oracle = fw.mkexc_s(
            'MissingArgumentUsageError',
            'EnumPos(find|do|mk|open|testf|path|rm|chldrn|xp|xr|x)',
        )
        self.autorun()

    def test_xp(self):
        self.args = ['xr']
        self.oracle = {
            'params': {
                'clapp.Action': 'SendRefToCcAction',
            }
        }
        self.autorun()

    def test_root_xp(self):
        self.args = ['-r', '/foo', 'xr']
        self.oracle = {
            'params': {
                'clapp.Action': 'SendRefToCcAction',
                'Root': '/foo',
            }
        }
        self.autorun()

    def test_root_twice_xp(self):
        self.args = ['-r', '/foo', '-r', '/bar', 'xr']
        self.oracle = {
            'params': {
                'clapp.Action': 'SendRefToCcAction',
                'Root': '/bar',
            }
        }
        self.autorun()

    def test_root_nopos(self):
        self.args = ['-r', '/foo']
        self.oracle = fw.mkexc_s(
            'MissingArgumentUsageError',
            'EnumPos(find|do|mk|open|testf|path|rm|chldrn|xp|xr|x)',
        )
        self.autorun()


class BmoWishCliTest(fw.SimpleHooverTest):

    # usage:
    #   bmo wish [options] WISH [WISH..]
    #   bmo wish [options] -c|-C
    #   bmo wish [options] -f [FILE..]
    #
    # options:
    #   -1, --first            Ignore all but first URL
    #   -B, --choose-browser   same as -b, but select browser interactively
    #   -Q, --choose-query     Same as -q, but select query interactively
    #   -a, --always-run       Always run browser, even without URL
    #   -b BIN, --browser BIN  Use browser BIN instead of auto-detection (using
    #                          bmo-sensible)
    #   -c, --primary-clipboard   Get wishes from primary clipboard (selection)
    #   -C, --clipboard        Get wishes from 'clipboard' clipboard (Ctrl-C)
    #   --secondary-clipboard  Get wishes from secondary clipboard
    #   -f [FILE..] --file [FILE..]   Get wishes from FILEs (standard input if
    #                          no FILE is provided or a file is '-')
    #   -l N, --limit N        Give up if there's more than N URLs to open
    #   -q QRY, --query QRY    Treat wishes as arguments to query QRY
    #
    # WISH can be URI, bookmark, search query followed by its arguments, or
    # body of text which is then searched by uripecker to find 'hidden' queries
    # or or URIs.  See uripecker documentation (`pydoc3 uripecker`) for
    # details.
    #
    # bad usage: no WISHes?
    #

    spec = Pattern(
        options={
            'First': Bool('-1,--first'),
            'ChooseBrowser': Bool('-B,--choose-browser'),
            'ChooseQuery': Bool('-Q,--choose-query'),
            'AlwaysRun': Bool('-a,--always-run'),
            'BrowserBin': KeyValue('-b,--browser'),
            'Limit': KeyValue('-l,--limit', factory=int),
            'Query': KeyValue('-q,--query'),
        },
        patterns=[
            Pattern(
                positionals=[
                    ('WISHES', NonemptyListPos(cond=C_NONOPT)),
                ],
                trigger={'clapp.Action': 'FulfillFromWishAction'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('-c|--primary-clipboard')),
                ],
                trigger={'clapp.Action': 'FulfillFromPrimaryClipboard'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('-C|--clipboard')),
                ],
                trigger={'clapp.Action': 'FulfillFromClipboardClipboard'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('--secondary-clipboard')),
                ],
                trigger={'clapp.Action': 'FulfillFromSecondaryClipboard'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('-f|--file')),
                    ('FILES', ListPos()),
                ],
                trigger={'clapp.Action': 'FulfillFromFile'},
            ),
            Pattern(
                positionals=[
                    ('_', LiteralPos('--help')),
                ],
                trigger={'clapp.Action': 'clapp.ShowHelpAction'},
            ),
        ]
    )

    def test_noargs(self):
        self.args = []
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_unknown_arg(self):
        self.args = ['--fist']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_helparg(self):
        self.args = ['--help']
        self.oracle = {
            'params': {
                '_': '--help',
                'clapp.Action': 'clapp.ShowHelpAction',
            },
        }
        self.autorun()

    def test_c_wish_1w(self):
        self.args = ['foo']
        self.oracle = {
            'params': {
                'clapp.Action': 'FulfillFromWishAction',
                'WISHES': ['foo'],
            },
        }
        self.autorun()

    def test_c_wish_Nw(self):
        self.args = ['foo', 'bar', 'baz', 'qux', 'quux']
        self.oracle = {
            'params': {
                'clapp.Action': 'FulfillFromWishAction',
                'WISHES': ['foo', 'bar', 'baz', 'qux', 'quux'],
            },
        }
        self.autorun()

    def test_c_wish_Ns(self):
        self.args = ['foo bar', 'baz', 'qux quux']
        self.oracle = {
            'params': {
                'clapp.Action': 'FulfillFromWishAction',
                'WISHES': self.args,
            },
        }
        self.autorun()

    def test_c_pclp_0w(self):
        self.args = ['-c']
        self.oracle = {
            'params': {
                '_': '-c',
                'clapp.Action': 'FulfillFromPrimaryClipboard',
            },
        }
        self.autorun()

    def test_c_pclp_1w(self):
        self.args = ['-c', 'foo']
        self.oracle = fw.mkexc_nupm(self.args)
        self.autorun()

    def test_c_cclp_0w(self):
        self.args = ['-C']
        self.oracle = {
            'params': {
                '_': '-C',
                'clapp.Action': 'FulfillFromClipboardClipboard',
            },
        }
        self.autorun()

    def test_cl_cclp_0w(self):
        self.args = ['--clipboard']
        self.oracle = {
            'params': {
                '_': '--clipboard',
                'clapp.Action': 'FulfillFromClipboardClipboard',
            },
        }
        self.autorun()

    def test_cl_sclp_0w(self):
        self.args = ['--secondary-clipboard']
        self.oracle = {
            'params': {
                '_': '--secondary-clipboard',
                'clapp.Action': 'FulfillFromSecondaryClipboard',
            },
        }
        self.autorun()

    def test_file_0w(self):
        self.args = ['-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'FILES': [],
            },
        }
        self.autorun()

    def test_file_long_0w(self):
        self.args = ['--file']
        self.oracle = {
            'params': {
                '_': '--file',
                'clapp.Action': 'FulfillFromFile',
                'FILES': [],
            },
        }
        self.autorun()

    def test_file_1w(self):
        self.args = ['-f', 'foo']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'FILES': self.args[1:],
            },
        }
        self.autorun()

    def test_file_long_1w(self):
        self.args = ['--file']
        self.oracle = {
            'params': {
                '_': '--file',
                'clapp.Action': 'FulfillFromFile',
                'FILES': self.args[1:],
            },
        }
        self.autorun()

    def test_file_Nw(self):
        self.args = ['-f', 'foo', 'bar']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'FILES': self.args[1:],
            },
        }
        self.autorun()

    def test_file_long_Nw(self):
        self.args = ['--file', '--foo', '--bar']
        self.oracle = {
            'params': {
                '_': '--file',
                'clapp.Action': 'FulfillFromFile',
                'FILES': self.args[1:],
            },
        }
        self.autorun()

    def test_onlyopt(self):
        self.args = ['-1']
        self.oracle = fw.mkexc_nupm([])
        self.autorun()

    def test_o_first(self):
        self.args = ['-1', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'First': True,
                'FILES': [],
            },
        }
        self.autorun()

    def test_o_choose_browser(self):
        self.args = ['-B', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'ChooseBrowser': True,
                'FILES': [],
            },
        }
        self.autorun()

    def test_o_choosequery(self):
        self.args = ['-Q', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'ChooseQuery': True,
                'FILES': [],
            },
        }
        self.autorun()

    def test_o_alwaysrun(self):
        self.args = ['-a', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'AlwaysRun': True,
                'FILES': [],
            },
        }
        self.autorun()

    def test_o_browserbin(self):
        self.args = ['-b', 'foo', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'BrowserBin': 'foo',
                'FILES': [],
            },
        }
        self.autorun()

    def test_o_browserbin_weird(self):
        self.args = ['-b', '--first', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'BrowserBin': '--first',
                'FILES': [],
            },
        }
        self.autorun()

    def test_o_limit(self):
        self.args = ['-l', '30', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'Limit': 30,
                'FILES': [],
            },
        }
        self.autorun()

    def test_o_limit_badval(self):
        self.args = ['-l', '3z0', '-f']
        self.oracle = fw.mkexc_s(
            'BadValueUsageError',
            "('Limit in KeyValue(-l|--limit)', 'int', '3z0')",
        )
        self.autorun()

    def test_o_query(self):
        self.args = ['-q', 'foo', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'Query': 'foo',
                'FILES': [],
            },
        }
        self.autorun()

    def test_ol_first(self):
        self.args = ['--first', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'First': True,
                'FILES': [],
            },
        }
        self.autorun()

    def test_ol_choose_browser(self):
        self.args = ['--choose-browser', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'ChooseBrowser': True,
                'FILES': [],
            },
        }
        self.autorun()

    def test_ol_choosequery(self):
        self.args = ['--choose-query', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'ChooseQuery': True,
                'FILES': [],
            },
        }
        self.autorun()

    def test_ol_alwaysrun(self):
        self.args = ['--always-run', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'AlwaysRun': True,
                'FILES': [],
            },
        }
        self.autorun()

    def test_ol_browserbin(self):
        self.args = ['--browser', 'foo', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'BrowserBin': 'foo',
                'FILES': [],
            },
        }
        self.autorun()

    def test_ol_limit(self):
        self.args = ['--limit', '30', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'Limit': 30,
                'FILES': [],
            },
        }
        self.autorun()

    def test_ol_query(self):
        self.args = ['--query', 'foo', '-f']
        self.oracle = {
            'params': {
                '_': '-f',
                'clapp.Action': 'FulfillFromFile',
                'Query': 'foo',
                'FILES': [],
            },
        }
        self.autorun()

    def test_allopts(self):
        self.args = [
            '-1',
            '-B',
            '-Q',
            '-a',
            '-b', 'vimb',
            '-l', '15',
            '-q', 'g',
            'foo', 'bar', 'baz',
        ]
        self.oracle = {
            'params': {
                'clapp.Action': 'FulfillFromWishAction',
                'First': True,
                'ChooseBrowser': True,
                'ChooseQuery': True,
                'AlwaysRun': True,
                'BrowserBin': 'vimb',
                'Query': 'g',
                'Limit': 15,
                'WISHES': ['foo', 'bar', 'baz'],
            },
        }
        self.autorun()

    def test_allopts_mixin_unknown(self):
        self.args = [
            '-1',
            '-B',
            '-Q',
            '-w',
            '-a',
            '-b', 'vimb',
            '-l', '15',
            '-q', 'g',
            'foo', 'bar', 'baz',
        ]
        self.oracle = fw.mkexc_nupm(self.args[3:])
        self.autorun()

    def test_allopts_then_unknown(self):
        self.args = [
            '-1',
            '-B',
            '-Q',
            '-a',
            '-b', 'vimb',
            '-l', '15',
            '-q', 'g',
            '--throng', 'bar', 'baz',
        ]
        self.oracle = fw.mkexc_nupm(self.args[10:])
        self.autorun()


