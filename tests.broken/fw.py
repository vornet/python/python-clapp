import unittest
import abc

import myatf
import myatf.desc

import clapp

import hoover


DEFAULT_SPEC_CASE_DATA: dict = {
    'params': {},
    'exc': None,
    'exc_data': None,
}


DEFAULT_OBJ_CASE_DATA: dict = {
    'obj': None,
    'class': 'object',
    'attr_names': [],
    'attr_items': [],
    'exc': None,
    'exc_data': None,
}


def mkexc_r(name, value):
    return {
        'exc': name,
        'exc_data': repr(value),
    }


def mkexc_s(name, value):
    return {
        'exc': name,
        'exc_data': str(value),
    }


def mkexc_nupm(args):
    return {
        'exc': 'UsageError',
        'exc_data': f"no usage pattern matched: {args!r}",
    }


def enum_argsets():
    cset = myatf.case.ArrayCaseSet(
        tpe=myatf.case.ArgOrValueType(),
        maxitems=11111,
    )
    return cset.items


class SpecTestDriver(hoover.TestDriver):

    def _get_data(self):
        data = DEFAULT_SPEC_CASE_DATA.copy()
        spec = self._args['spec']
        args = self._args['args']
        try:
            data['params'] = spec.parse_all(args)
        except clapp.UsageError as e:
            data['exc'] = e.__class__.__name__
            data['exc_data'] = str(e)
        self.data = data


class ObjTestDriver(hoover.TestDriver):

    @abc.abstractmethod
    def is_expectable(self, exc):
        pass

    @abc.abstractmethod
    def make_object(self, *args, **kwargs):
        pass

    def __pull_exc_info(self, exc):
        return {
            'exc': exc.__class__.__name__,
            'exc_data': str(exc),
        }

    def __pull_obj_info(self, obj):
        def is_public(K):
            return not K.startswith('_')
        cls = obj.__class__
        cls_attrs = set(dir(cls))
        obj_attrs = set(dir(obj))
        new_attrs = sorted(obj_attrs - cls_attrs)
        attr_items = [
            (a, getattr(obj, a))
            for a in new_attrs
        ]
        return {
            'class': cls.__name__,
            'attr_names': new_attrs,
            'attr_items': attr_items,
        }

    def __make_obj(self, *args, **kwargs):
        data = {}
        try:
            obj = self.make_object(*args, **kwargs)
            data = self.__pull_obj_info(obj)
        except Exception as e:
            if not self.is_expectable(e):
                raise e
            data = self.__pull_exc_info(e)
        return DEFAULT_OBJ_CASE_DATA | data

    def _get_data(self):
        data = DEFAULT_OBJ_CASE_DATA.copy()
        args = self._args['args']
        kwargs = self._args['kwargs']
        data.update(self.__make_obj(*args, **kwargs))
        self.data = data


class SpecOracleDriver(hoover.TestDriver, metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def make_oracle(self, args: list[str]) -> dict:
        return {}

    def _get_data(self):
        oracle = DEFAULT_SPEC_CASE_DATA | self.make_oracle(self._args['args'])
        if oracle.get('exc'):
            oracle['params'] = {}
        self.data = oracle


class FakeOracleDriver(hoover.TestDriver):
    """
    Just takes oracle from the args
    """

    default_case_data: dict = {}

    def _autocomplete_oracle(self):
        pass

    def _get_data(self):
        if 'oracle' not in self._args:
            raise ValueError(f"no oracle in case: {self._args}")
        self.data = self.default_case_data | self._args['oracle']
        self._autocomplete_oracle()


class FakeSpecOracleDriver(FakeOracleDriver):

    default_case_data = DEFAULT_SPEC_CASE_DATA


class FakeObjOracleDriver(FakeOracleDriver):

    default_case_data = DEFAULT_OBJ_CASE_DATA


class HooverTest(unittest.TestCase, metaclass=abc.ABCMeta):

    driver_settings = None
    only_own = None
    match_op = None
    hacks = None
    goal = None

    def run_hoover_plan(self, csrc):
        plan = hoover.TestPlan(
            csrc=csrc,
            rclass=self.result_driver,
            oclass=self.oracle_driver,
            driver_settings=self.driver_settings,
            only_own=self.only_own,
            match_op=self.match_op,
            hacks=self.hacks,
            goal=self.goal,
        )
        report = plan.run()
        if report.errors_found():
            self.fail(report.format_report())

    def run_hoover_case(self, case):
        self.run_hoover_plan([case])

    result_driver = SpecTestDriver


class CartmanHooverTest(HooverTest):

    @property
    @abc.abstractmethod
    def argsrc(self):
        pass

    @property
    @abc.abstractmethod
    def scheme(self):
        pass

    def test_using_rt(self):
        self.run_hoover_plan(
            hoover.Cartman(self.argsrc, self.scheme)
        )


class SimpleHooverTest(HooverTest):
    """
    Single spec/args/oracle per test method.

    Test methods only need to set self.args, self.oracle and
    self.spec (the latter can be set as class property) and
    call self.autorun().

    This class will do the rest: wrap things in hoover.TestPlan
    with SpecTestDriver and FakeSpecOracleDriver, run the plan.
    extract result, etc.
    """

    oracle_driver = FakeSpecOracleDriver

    def autorun(self):
        self.run_hoover_case({
            'spec': self.spec,
            'args': self.args,
            'oracle': self.oracle,
        })


class SimpleObjHooverTest(HooverTest):
    """
    Single args/oracle per test method.

    Test methods only need to set self.args and self.oracle
    and self.spec (the latter can be set as class property) and
    call self.autorun().

    This class will do the rest: wrap things in hoover.TestPlan
    with SpecTestDriver and FakeSpecOracleDriver, run the plan.
    extract result, etc.
    """

    oracle_driver = FakeObjOracleDriver
    result_driver = ObjTestDriver

    args = None
    kwargs = None

    def autorun(self):
        self.run_hoover_case({
            'args': self.args or [],
            'kwargs': self.kwargs or {},
            'oracle': self.oracle,
        })

