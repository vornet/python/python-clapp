import unittest

from tests import fw

import clapp.app


def normalize(value):
    if value is Bar:
        return 'our Bar class'
    if value.__class__ is clapp.app.Params:
        return str(value)
    if value.__class__ is object:
        return 'some object()'
    if value.__class__ is Bar:
        return str(value)
    return value


def normalize_items(items):
    return sorted([
        (k, normalize(v))
        for k, v in
        items
    ])


class Bar:

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f'Bar(value={self.value})'


class FakeParamsOracleDriver(fw.FakeObjOracleDriver):

    def _normalize_data(self):
        self.data['attr_items'] = normalize_items(self.data['attr_items'])

    default_case_data = fw.DEFAULT_OBJ_CASE_DATA | {'class': 'Params'}


class ParamTestDriver(fw.ObjTestDriver):

    def _normalize_data(self):
        self.data['attr_items'] = normalize_items(self.data['attr_items'])

    def make_object(self, *args, **kwargs):
        return clapp.app.Params(*args, **kwargs)

    def is_expectable(self, e):
        return e.__class__.__name__ in [
            'ParamsError',
            'AppBug',
        ]


class ParamsTest(fw.SimpleObjHooverTest):

    result_driver = ParamTestDriver
    oracle_driver = FakeParamsOracleDriver

    def test_empty(self):
        self.args = [{}]
        self.oracle = {
            'attr_names': [],
        }
        self.autorun()

    def test_simple_str(self):
        self.args = [{'foo': 'bar'}]
        self.oracle = {
            'attr_names': ['foo'],
            'attr_items': [('foo', 'bar')],
        }
        self.autorun()

    def test_simple_int(self):
        self.args = [{'foo': 1}]
        self.oracle = {
            'attr_names': ['foo'],
            'attr_items': [('foo', 1)],
        }
        self.autorun()

    def test_simple_list(self):
        self.args = [{'foo': ['bar', 'baz']}]
        self.oracle = {
            'attr_names': ['foo'],
            'attr_items': [('foo', ['bar', 'baz'])],
        }
        self.autorun()

    def test_simple_tuple(self):
        self.args = [{'foo': ('bar', 'baz')}]
        self.oracle = {
            'attr_names': ['foo'],
            'attr_items': [('foo', ('bar', 'baz'))],
        }
        self.autorun()

    def test_simple_dict(self):
        self.args = [{'foo': {'bar': 'baz'}}]
        self.oracle = {
            'attr_names': ['foo'],
            'attr_items': [('foo', {'bar': 'baz'})],
        }
        self.autorun()

    def test_simple_builtin_obj(self):
        self.args = [{'foo': object()}]
        self.oracle = {
            'attr_names': ['foo'],
            'attr_items': [('foo', 'some object()')],
        }
        self.autorun()

    def test_simple_normal_obj(self):
        self.args = [{'foo': Bar('baz')}]
        self.oracle = {
            'attr_names': ['foo'],
            'attr_items': [('foo', 'Bar(value=baz)')],
        }
        self.autorun()

    def test_simple_class_obj(self):
        self.args = [{'foo': Bar}]
        self.oracle = {
            'attr_names': ['foo'],
            'attr_items': [('foo', 'our Bar class')],
        }
        self.autorun()

    def test_dotted(self):
        self.args = [{
            'foo.bar': 'baz',
            'qux': 'quux',
        }]
        self.oracle = {
            'attr_names': ['foo', 'qux'],
            'attr_items': [
                ('foo', 'Params(bar=baz)'),
                ('qux', 'quux'),
            ],
        }
        self.autorun()

    def test_dashbgn(self):
        self.args = [{
            '_foo': 'bar',
            'qux': 'quux',
        }]
        self.oracle = {
            'attr_names': ['_foo', 'qux'],
            'attr_items': [
                ('_foo', 'bar'),
                ('qux', 'quux'),
            ],
        }
        self.autorun()

    def test_dotbgn(self):
        self.args = [{
            '.bar': 'baz',
            'qux': 'quux',
        }]
        self.oracle = {
            'attr_names': ['qux'],
            'attr_items': [
                ('qux', 'quux'),
            ],
        }
        self.autorun()

    def test_dotend(self):
        self.args = [{
            'foo.': 'baz',
            'qux': 'quux',
        }]
        self.oracle = {
            'attr_names': ['qux'],
            'attr_items': [
                ('qux', 'quux'),
            ],
        }
        self.autorun()

    def test_deep(self):
        self.args = [{
            'shop.bar.alice': 'cashier',
            'shop.bar.bob': 'barista',
            'shop.storage': 'nobody',
        }]
        self.oracle = {
            'attr_names': ['shop'],
            'attr_items': [
                ('shop',
                 'Params('
                 'bar=Params(alice=cashier,bob=barista)'
                 ',storage=nobody'
                 ')'),
            ],
        }
        self.autorun()

    def test_lvl_conflict(self):
        self.args = [{
            'foo.bar.baz': 'mcintyre',
            'foo.bar': 'basista',
            'qux': 'quux',
        }]
        self.oracle = {
            'attr_names': ['foo', 'qux'],
            'attr_items': [
                ('qux', 'quux'),
                ('foo', 'Params(bar=Params(baz=mcintyre))'),
            ],
        }
        self.autorun()
